# TBFT开源共识引擎

TBFT开源引擎就是使用TBFT共识算法作为核心的开源引擎架构，它的目标是允许开发者轻松创建自己定义的基于TBFT共识算法的区块链。TBFT引擎只专注于共识本身，不关心共识之外的其他实现，如网络通信、验签、共识内容、持久化等。开发者可以实现引擎对外提供的接口，启动自定义的节点，从而能够快速搭建一条基于TBFT共识算法实现的区块链。
- [TBFT开源引擎架构概述](https://git.code.tencent.com/ChainMaker/tbft-engine/blob/develop/docs/TBFT%E5%BC%80%E6%BA%90%E5%BC%95%E6%93%8E%E6%9E%B6%E6%9E%84%E6%A6%82%E8%BF%B0.md)
- [examples](https://git.code.tencent.com/ChainMaker/tbft-engine/tree/develop/examples)