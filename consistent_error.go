/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 SPDX-License-Identifier: Apache-2.0
*/

package tbft_engine

import "errors"

var (
	ErrorInvalidParameter  = errors.New("invalid parameter")
	ErrorBroadcasterExist  = errors.New("broadcaster exist")
	ErrorDecoderExist      = errors.New("decoder exist")
	ErrorNetHandlerExist   = errors.New("netHandler exist")
	ErrorInterceptorExist  = errors.New("interceptor exist")
	ErrorRunRepeatedly     = errors.New("run repeatedly")
	ErrorNotRunning        = errors.New("not running")
	ErrorRemoterExist      = errors.New("remoter exist")
	ErrorRemoterNotExist   = errors.New("remoter not exist")
	ErrorRemoterEqualLocal = errors.New("remoter equal local")
)
