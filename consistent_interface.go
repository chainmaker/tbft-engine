/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 SPDX-License-Identifier: Apache-2.0
*/

package tbft_engine

import (
	"context"
)

// ConsistentEngine 一致性引擎，用于节点间（共识状态）信息同步
type ConsistentEngine interface {

	// Start 启动一致性引擎
	// ctx 后续扩展用，启动失败返回error
	Start(ctx context.Context) error

	// Stop 停止一致性引擎
	// ctx 后续扩展用，，停止失败返回error
	Stop(ctx context.Context) error

	// AddBroadcaster 添加状态广播器（如一个tbft状态广播器）
	// id 广播器标识，需要保证唯一性
	// broadcast 广播器，需要用户自己实现（如：tbft广播器、maxbft广播器）
	AddBroadcaster(id string, broadcast *StatusBroadcaster) error

	// UpdateNodeStatus 更新本地状态
	// id 节点标识，需要保证唯一性
	// node 需要更新的节点信息（包含节点状态，节点状态可以有多种）
	UpdateNodeStatus(id string, node *Node) error

	// PutRemoter 添加节点
	// id 节点标识，需要保证唯一性
	// node 需要添加的节点信息（包含节点状态，节点状态可以有多种）
	PutRemoter(id string, node *Node) error

	// RemoveRemoter 删除节点
	// id 节点标识，当节点不存在时返回错误消息
	RemoveRemoter(id string) error

	// RegisterStatusCoder 注册状态解析器
	// decoderType 解析器标识，需要保证唯一性
	// decoder 需要添加的解析器，由用户实现（如：tbft解析器）
	RegisterStatusCoder(decoderType int8, decoder *StatusDecoder) error

	// RegisterStatusInterceptor 注册过滤器
	// interceptorType 过滤器标识，需要保证唯一性
	// interceptor 需要添加的过滤器，由用户实现（如：tbft过滤器）
	RegisterStatusInterceptor(interceptorType int8, interceptor *StatusInterceptor) error
	//
	RegisterNetHandle(netHandler NetHandler) error
}

// Message 用户一致性引擎于其他模块（如tbft/maxbft）数据交互
type Message interface {

	// Send 一致性引擎对外发送数据
	// payload 需要发送的消息
	Send(payload interface{})

	// Receive 一致性引擎接收外部数据
	// 返回接收到的消息
	Receive() interface{}

	AddNetMsg(interface{})
}

// Broadcast 广播器
type Broadcast func(*Node, *Node) (interface{}, error)

// Status 节点共识状态
type Status interface {

	// Type 状态类型
	Type() int8

	// Data 状态内容
	Data() interface{}

	// Update 更新状态
	Update(status Status)
}
