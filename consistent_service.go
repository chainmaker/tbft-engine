/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 SPDX-License-Identifier: Apache-2.0
*/

package tbft_engine

import (
	"context"
	"time"

	tbftpb "chainmaker.org/chainmaker/tbft-engine/pb"
)

// StatusConsistentEngine 一致性引擎实现
type StatusConsistentEngine struct {
	// log  [Consistent]
	log Logger
	//状态广播器
	broadcasters map[string]*StatusBroadcaster
	//本地节点信息
	local *Node
	//远端节点信息
	remoters map[string]*Node
	//解析器
	decoders *StatusDecoder
	// 网络处理器
	netHandler NetHandler
	//拦截器
	interceptors map[int8]*StatusInterceptor
	//发送/接收消息
	msg Message
	//运行状态
	running bool
}

// NewConsistentService nolint: unused
func NewConsistentService(local *Node,
	msg Message,
	log Logger) *StatusConsistentEngine {
	bcs := StatusConsistentEngine{
		broadcasters: make(map[string]*StatusBroadcaster),
		remoters:     make(map[string]*Node),
		interceptors: make(map[int8]*StatusInterceptor),
		log:          log,
		local:        local,
		msg:          msg,
		running:      false,
	}
	log.Debugf("local id: %s", local.ID())

	return &bcs
}

func (e *StatusConsistentEngine) AddBroadcaster(id string, broadcaster *StatusBroadcaster) error {

	if id == "" || broadcaster == nil || id != broadcaster.ID() {
		return ErrorInvalidParameter
	}
	if e.broadcasters[id] != nil {
		return ErrorBroadcasterExist
	}
	e.broadcasters[id] = broadcaster
	e.log.Infof("AddBroadcaster(%s) succeed", id)

	return nil
}

// UpdateNodeStatus 更新本地状态
func (e *StatusConsistentEngine) UpdateNodeStatus(id string, node *Node) error {
	if id == "" || node == nil {
		return ErrorInvalidParameter
	}

	e.log.Infof("UpdateNodeStatus:%s", id)
	// 拦截器
	for _, interceptor := range e.interceptors {
		for _, status := range node.Statuses() {
			err := interceptor.Handle(status)
			if err != nil {
				return err
			}
		}

	}

	// 更新节点状态
	if id == e.local.ID() {
		e.local = node

	} else {
		if e.remoters[id] != nil {
			e.remoters[id] = node
		}
	}
	return nil
}

func (e *StatusConsistentEngine) PutRemoter(id string, node *Node) error {
	if id == "" || node == nil {
		return ErrorInvalidParameter
	}

	e.log.Infof("AddRemoter:%s", id)

	if e.local.ID() == id {
		return ErrorRemoterEqualLocal
	}
	if e.remoters[id] != nil {
		return ErrorRemoterExist
	}
	e.remoters[id] = node
	e.log.Debugf("after AddRemoter,remoters size is %d", len(e.remoters))

	return nil
}

func (e *StatusConsistentEngine) RemoveRemoter(id string) error {
	if id == "" {
		return ErrorInvalidParameter
	}

	e.log.Infof("DelRemoter:%s", id)
	if e.remoters[id] == nil {
		e.log.Warnf("DelRemoter: %s is not exist", id)
		return ErrorRemoterNotExist
	}

	delete(e.remoters, id)

	e.log.Debugf("after DelRemoter,remoters size is %d", len(e.remoters))

	return nil
}

func (e *StatusConsistentEngine) RegisterStatusCoder(decoder *StatusDecoder) error {
	e.log.Info("RegisterStatusCoder:")
	if e.decoders != nil {
		//已经存在
		e.log.Warnf("RegisterStatusCoder(%d) is exist")
		return ErrorDecoderExist
	}
	e.decoders = decoder

	return nil
}
func (e *StatusConsistentEngine) RegisterNetHandle(netHandler NetHandler) error {
	e.log.Info("RegisterNetHandle:")
	if e.netHandler != nil {
		//已经存在
		e.log.Warnf("RegisterNetHandle(%d) is exist")
		return ErrorNetHandlerExist
	}
	e.netHandler = netHandler

	return nil
}

func (e *StatusConsistentEngine) RegisterStatusInterceptor(interceptorType int8, interceptor *StatusInterceptor) error {
	if interceptor == nil {
		return ErrorInvalidParameter
	}

	if e.interceptors[interceptorType] != nil {
		return ErrorInterceptorExist
	}
	e.log.Infof("RegisterStatusInterceptor, size before:%d", len(e.interceptors))
	e.interceptors[interceptorType] = interceptor

	return nil
}

func (e *StatusConsistentEngine) handleReceiveMessage() {
	e.log.Debugf("start handleReceiveMessage")
	for {
		if !e.running {
			continue
		}
		m := e.msg.Receive()
		if m == nil {
			e.log.Debugf("receive nil")
			continue
		}
		e.log.Debugf("receive payload")

		data := e.decoders.Decode(m)
		rs, ok := data.(*Node)
		if !ok {
			continue
		}

		remoteInfo := e.remoters[rs.ID()]
		if remoteInfo == nil {
			e.log.Errorf("error message, node[%s] not found", rs.ID())
			continue
		}

		e.log.Debugf("update status, remoter id is %s", rs.ID())
		for _, v := range rs.Statuses() {
			e.log.Debugf("status type:%d", v.Type())
			remoteInfo.UpdateStatus(v)
		}

	}
}

func (e *StatusConsistentEngine) Start(ctx context.Context) error {
	if e.running {
		return ErrorRunRepeatedly
	}
	if ctx != nil && ctx.Err() != nil {
		e.log.Debugf(ctx.Err().Error())
	}

	// 处理接收到的消息
	go e.handleReceiveMessage()
	// 通过ticker方式，间隔一定时间，广播消息
	err := e.handle()
	if err != nil {
		return err
	}

	// 设置一致性引擎为运行状态
	e.running = true

	return nil
}

func (e *StatusConsistentEngine) tickerHandle(
	broadcaster *StatusBroadcaster, interval interface{}) {
	t, ok := interval.(time.Duration)
	if !ok {
		return
	}
	ticker := time.NewTicker(t)
	// 每间隔一定时间，通过PreBroadcaster()函数判断是否需要给其他节点发送消息
	for range ticker.C {
		if !broadcaster.IsRunning() {
			continue
		}
		preBroadcast := broadcaster.PreBroadcaster()
		// 遍历远端节点列表
		for _, v := range e.remoters {
			netMSGs, err := preBroadcast(e.local, v)
			if err != nil {
				e.log.Errorf("preBroadcast err:%s", err)
				continue
			}

			nms, ok := netMSGs.([]*tbftpb.NetMsg)
			if !ok {
				e.log.Errorf("netMSGs is't []*tbftpb.NetMsg")
				continue
			}
			for n := 0; n < len(nms); n++ {
				e.log.Debugf("send msg(to:%s,payload.len:%s)", nms[n].To, len(nms[n].String()))
				e.msg.Send(nms[n])
			}

		}
	}
}

func (e *StatusConsistentEngine) handle() error {
	e.log.Infof("StatusConsistentEngine Start")

	for _, broadcaster := range e.broadcasters {
		if !broadcaster.IsRunning() {
			e.log.Infof("broadcasters[%s] Start", broadcaster.ID())

			err := broadcaster.Start()
			if err != nil {
				return err
			}

			go e.tickerHandle(broadcaster, broadcaster.TimePattern())
		}
	}

	return nil
}

func (e *StatusConsistentEngine) Stop(ctx context.Context) error {
	if !e.running {
		return ErrorNotRunning
	}
	if ctx != nil && ctx.Err() != nil {
		e.log.Debugf(ctx.Err().Error())
	}
	e.log.Infof("StatusConsistentEngine Close")

	for _, v := range e.broadcasters {
		if v.IsRunning() {
			err := v.Stop()
			if err != nil {
				return err
			}
		}
	}
	e.running = false

	return nil
}
