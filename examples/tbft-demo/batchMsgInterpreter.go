package tbft_demo

import (
	tbftengine "chainmaker.org/chainmaker/tbft-engine"
	"math/rand"
)

var _ tbftengine.BatchMsgInterpreter = &BatchMsgInterpreter{}
var _ tbftengine.BatchMsg = &BatchMsg{}

var (
	dataSize = 1024 * 10000
)

type BatchMsgInterpreter struct {
	Sequence chan uint64
	nodeId   string
	messageC chan *tbftengine.ProposalBatchMsg
}

func NewBatchMsgInterpreter(size int, nodeId string) *BatchMsgInterpreter {
	b := &BatchMsgInterpreter{
		nodeId:   nodeId,
		messageC: make(chan *tbftengine.ProposalBatchMsg, size),
		Sequence: make(chan uint64),
	}
	go b.proposeBatch()
	return b
}

func (b *BatchMsgInterpreter) proposeBatch() {
	for {
		select {
		case index := <-b.Sequence:
			payload := make([]byte, dataSize)
			batchMsgHash := make([]byte, 32)
			if _, err := rand.Read(payload); err != nil {
				panic(err)
			}
			if _, err := rand.Read(batchMsgHash); err != nil {
				panic(err)
			}
			proposalBatchMsg := &tbftengine.ProposalBatchMsg{
				BatchMsg: &BatchMsg{
					Proposer: b.nodeId,
					Index:    index + 1,
					Hash:     batchMsgHash,
					Data:     payload},
			}
			b.messageC <- proposalBatchMsg
		}
	}
}

func (b *BatchMsgInterpreter) SetSequence(sequence uint64) {
	b.Sequence <- sequence
}

func (b *BatchMsgInterpreter) Name() string {
	return BatchMsgInterpreterName
}

func (b *BatchMsgInterpreter) Close() {
}

// PrepareBatchMsg TBFT引擎通过监听此chan, 获取一个BatchMsg进行共识
func (b *BatchMsgInterpreter) PrepareBatchMsg() <-chan *tbftengine.ProposalBatchMsg {
	return b.messageC
}

// Block 共识内容
type BatchMsg struct {
	Proposer string
	Index    uint64
	Hash     []byte
	Data     []byte
	Sign     []byte
}

func (b *BatchMsg) Sequence() uint64 {
	return b.Index
}

func (b *BatchMsg) Key() []byte {
	return b.Hash
}

// 获取BatchMsg签名
func (b *BatchMsg) GetSetSignature() interface{} {
	return b.Sign
}

// 设置BatchMsg签名
func (b *BatchMsg) SetSignature(sign interface{}) {
	bytesSign, ok := sign.([]byte)
	if ok {
		b.Sign = bytesSign
	}
}
