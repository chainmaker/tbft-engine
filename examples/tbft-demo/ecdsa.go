package tbft_demo

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"math/big"
	"strings"
)

type EcdsaKeyPair struct {
	PrivateKey *ecdsa.PrivateKey
	PubKey     *ecdsa.PublicKey
}

func NewEcdsaKeyPair(privateKeyStr, publicKeyStr string) *EcdsaKeyPair {
	privateKey, err := BuildPrivateKey(privateKeyStr)
	if err != nil {
		return nil
	}

	pubKey, err := BuildPublicKey(publicKeyStr)
	if err != nil {
		return nil
	}
	return &EcdsaKeyPair{
		PrivateKey: privateKey,
		PubKey:     pubKey,
	}
}

// 生成公私钥
func GenKeyPair() (privateKey string, publicKey string, e error) {
	// GenerateKey生成公私钥对。
	// priKey --- priv.PublicKey.X, priv.PublicKey.Y
	priKey, e := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if e != nil {
		return "", "", e
	}
	// 将一个EC私钥转换为SEC 1, ASN.1 DER格式。
	ecPrivateKey, e := x509.MarshalECPrivateKey(priKey)
	if e != nil {
		return "", "", e
	}
	// 私钥
	privateKey = base64.StdEncoding.EncodeToString(ecPrivateKey)

	X := priKey.X
	Y := priKey.Y
	xStr, e := X.MarshalText()
	if e != nil {
		return "", "", e
	}
	yStr, e := Y.MarshalText()
	if e != nil {
		return "", "", e
	}
	public := string(xStr) + "+" + string(yStr)
	// 公钥 x+y
	publicKey = base64.StdEncoding.EncodeToString([]byte(public))
	return
}

// 解析私钥
func BuildPrivateKey(privateKeyStr string) (priKey *ecdsa.PrivateKey, e error) {
	bytes, e := base64.StdEncoding.DecodeString(privateKeyStr)
	if e != nil {
		return nil, e
	}
	// ParseECPrivateKey解析SEC 1, ASN.1 DER形式的EC私钥。
	priKey, e = x509.ParseECPrivateKey(bytes)
	if e != nil {
		return nil, e
	}
	return
}

// 解析公钥
func BuildPublicKey(publicKeyStr string) (pubKey *ecdsa.PublicKey, e error) {
	bytes, e := base64.StdEncoding.DecodeString(publicKeyStr)
	if e != nil {
		return nil, e
	}
	split := strings.Split(string(bytes), "+")
	xStr := split[0]
	yStr := split[1]
	x := new(big.Int)
	y := new(big.Int)
	e = x.UnmarshalText([]byte(xStr))
	if e != nil {
		return nil, e
	}
	e = y.UnmarshalText([]byte(yStr))
	if e != nil {
		return nil, e
	}
	pub := ecdsa.PublicKey{Curve: elliptic.P256(), X: x, Y: y}
	pubKey = &pub
	return
}

func hash(data []byte) []byte {
	sum := sha256.Sum256(data)
	return sum[:]
}

// 签名
func (ekp *EcdsaKeyPair) Sign(content []byte) (signature []byte, e error) {
	// 随机数，用户私钥，hash签署消息
	r, s, e := ecdsa.Sign(rand.Reader, ekp.PrivateKey, hash(content))
	if e != nil {
		return nil, e
	}
	rt, e := r.MarshalText()
	st, e := s.MarshalText()
	// r+s
	signStr := string(rt) + "+" + string(st)
	signature = []byte(signStr)
	return
}

// 验签
func (ekp *EcdsaKeyPair) VerifySign(content []byte, signature []byte, publicKey *ecdsa.PublicKey) bool {
	// +号 签名 切片
	split := strings.Split(string(signature), "+")
	rStr := split[0]
	sStr := split[1]
	rr := new(big.Int)
	ss := new(big.Int)
	e := rr.UnmarshalText([]byte(rStr))
	if e != nil {
		return false
	}
	e = ss.UnmarshalText([]byte(sStr))
	if e != nil {
		return false
	}
	// 验签
	return ecdsa.Verify(publicKey, hash(content), rr, ss)
}
