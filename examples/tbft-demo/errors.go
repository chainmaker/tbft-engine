package tbft_demo

import (
	"errors"
)

var (
	ErrInvalidMsg   = errors.New("received invalid msg")
	ErrNotExistCode = errors.New("response code not found")
	ErrInvaildCode  = errors.New("response code is not 200")
)
