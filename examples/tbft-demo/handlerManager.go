package tbft_demo

import (
	"errors"
	tbftengine "chainmaker.org/chainmaker/tbft-engine"
)

const (
	BatchMsgInterpreterName = "batchMsgInterpreter"
	CommitterName           = "committer"
	NetHandlerName          = "netHandler"
	ParamsHandlerName       = "paramsHandler"
	VerifierName            = "verifier"
	WalHandlerName          = "walHandler"
	ProposalHandlerName     = "ProposalHandlerName"
)

var (
	ErrErrorHandler   = errors.New("the handler initialization error ")
	ErrNoExistHandler = errors.New("the handler does not exist")
)

type Handler interface {
	Name() string
	Close()
}

type HandlerManager struct {
	logger      *Logger
	host        *tbftengine.ConsensusTBFTImpl
	handlerFunc map[string]func() error
	HandlerMap  map[string]Handler
}

func NewHandlerManager(logger *Logger, host *tbftengine.ConsensusTBFTImpl, hs ...Handler) *HandlerManager {
	handlerManager := &HandlerManager{}
	HandlerMap := make(map[string]Handler)
	for _, h := range hs {
		HandlerMap[h.Name()] = h
	}

	handlerFunc := map[string]func() error{
		BatchMsgInterpreterName: handlerManager.registerBatchMsgInterpreter,
		CommitterName:           handlerManager.registeCommitter,
		NetHandlerName:          handlerManager.registerNetHandler,
		ParamsHandlerName:       handlerManager.registerParamsHandler,
		VerifierName:            handlerManager.registerVerifierName,
		WalHandlerName:          handlerManager.registerWalHandler,
		ProposalHandlerName:     handlerManager.registerProposalHandler,
	}

	handlerManager.logger = logger
	handlerManager.host = host
	handlerManager.HandlerMap = HandlerMap
	handlerManager.handlerFunc = handlerFunc
	return handlerManager
}

// StartrHandler start all handlers in the  handlerManager
func (hm *HandlerManager) StartrHandler() error {
	for _, f := range hm.handlerFunc {
		err := f()
		if err != nil {
			hm.logger.Errorf("handlerManager registerHandler failed , err = %v", err)
			return err
		}
	}
	return nil
}

// StopHandler stop all the handlers in handlerManager
func (hm *HandlerManager) StopHandler() {
	for _, h := range hm.HandlerMap {
		h.Close()
	}
}

// registerNetHandler implements the register netHandler
func (hm *HandlerManager) registerNetHandler() error {
	if hm.HandlerMap[NetHandlerName] == nil {
		return nil
	}
	netHandler, ok := hm.HandlerMap[NetHandlerName].(*NetHandler)
	if !ok {
		return ErrErrorHandler
	}
	_ = hm.host.RegisterNetHandler(netHandler)
	return nil
}

// registerWalHandler implements the register walHandler
func (hm *HandlerManager) registerWalHandler() error {
	if hm.HandlerMap[WalHandlerName] == nil {
		return nil
	}
	walHandler, ok := hm.HandlerMap[WalHandlerName].(*WalHandler)
	if !ok {
		return ErrErrorHandler
	}
	_ = hm.host.RegisterWalHandler(walHandler)
	return nil
}

// registerBatchMsgInterpreter implements the register batchMsgInterpreter
func (hm *HandlerManager) registerBatchMsgInterpreter() error {
	if hm.HandlerMap[BatchMsgInterpreterName] == nil {
		return nil
	}
	batchMsgInterpreter, ok := hm.HandlerMap[BatchMsgInterpreterName].(*BatchMsgInterpreter)
	if !ok {
		return ErrErrorHandler
	}
	_ = hm.host.RegisterInterpreter(batchMsgInterpreter)
	return nil
}

// registeCommitter implements the register committer
func (hm *HandlerManager) registeCommitter() error {
	if hm.HandlerMap[CommitterName] == nil {
		return nil
	}
	committer, ok := hm.HandlerMap[CommitterName].(*Committer)
	if !ok {
		return ErrErrorHandler
	}
	_ = hm.host.RegisterCommitter(committer)
	return nil
}

// registerParamsHandler implements the register paramsHandler
func (hm *HandlerManager) registerParamsHandler() error {
	if hm.HandlerMap[ParamsHandlerName] == nil {
		return nil
	}
	paramsHandler, ok := hm.HandlerMap[ParamsHandlerName].(*ParamsHandler)
	if !ok {
		return ErrErrorHandler
	}
	_ = hm.host.RegisterParamsHandler(paramsHandler)
	return nil
}

// registerVerifierName implements the register verifier
func (hm *HandlerManager) registerVerifierName() error {
	if hm.HandlerMap[VerifierName] == nil {
		return nil
	}
	verifier, ok := hm.HandlerMap[VerifierName].(*Verifier)
	if !ok {
		return ErrErrorHandler
	}
	_ = hm.host.RegisterVerifyHandler(verifier)
	return nil
}

// registerWalHandler implements the register walHandler
func (hm *HandlerManager) registerProposalHandler() error {
	if hm.HandlerMap[ProposalHandlerName] == nil {
		return nil
	}
	proposalHandlerName, ok := hm.HandlerMap[ProposalHandlerName].(*Coder)
	if !ok {
		return ErrErrorHandler
	}
	_ = hm.host.RegisterProposalCoder(proposalHandlerName)
	return nil
}
