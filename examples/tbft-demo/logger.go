package tbft_demo

import (
	"fmt"
	"github.com/wonderivan/logger"
)

var logJson = `{
	"TimeFormat":"2006-01-02 15:04:05",
	"Console": {
		"level": "EMER",
		"color": false
	},
	"File": {
		"filename": "default.log",
		"level": "INFO",
		"daily": true,
		"maxlines": 1000000,
		"maxsize": 100,
		"maxdays": -1,
		"append": true,
		"permit": "0660"
	}
}`

type Logger struct {
}

func NewLogger() *Logger {
	logger.SetLogger(logJson)
	return &Logger{}
}

func getMessage(template string, fmtArgs []interface{}) string {
	if len(fmtArgs) == 0 {
		return template
	}

	if template != "" {
		return fmt.Sprintf(template, fmtArgs...)
	}

	if len(fmtArgs) == 1 {
		if str, ok := fmtArgs[0].(string); ok {
			return str
		}
	}
	return fmt.Sprint(fmtArgs...)
}

func (log *Logger) Debugf(format string, args ...interface{}) {
	logger.Debug(getMessage(format, args))
}

func (log *Logger) Debug(args ...interface{}) {
	logger.Debug(getMessage("", args))
}

func (log *Logger) Debugw(msg string, keysAndValues ...interface{}) {
	return
}

func (log *Logger) Error(args ...interface{}) {
	logger.Error(getMessage("", args))
}

func (log *Logger) Errorf(format string, args ...interface{}) {
	logger.Error(getMessage(format, args))
}

func (log *Logger) Errorw(msg string, keysAndValues ...interface{}) {
	return
}

func (log *Logger) Warn(args ...interface{}) {
	logger.Warn(getMessage("", args))
}

func (log *Logger) Warnf(format string, args ...interface{}) {
	logger.Warn(getMessage(format, args))
}

func (log *Logger) Warnw(msg string, keysAndValues ...interface{}) {
	return
}

func (log *Logger) Info(args ...interface{}) {
	logger.Info(getMessage("", args))
}

func (log *Logger) Infof(format string, args ...interface{}) {
	logger.Info(getMessage(format, args))
}

func (log *Logger) Infow(msg string, keysAndValues ...interface{}) {
	return
}

func (log *Logger) Fatal(args ...interface{}) {
	logger.Fatal(getMessage("", args))
}

func (log *Logger) Fatalf(format string, args ...interface{}) {
	logger.Fatal(getMessage(format, args))
}

func (log *Logger) Fatalw(msg string, keysAndValues ...interface{}) {
	return
}
