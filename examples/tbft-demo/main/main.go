package main

import (
	"tbft-engine/examples/tbft-demo"
	"time"
)

func main() {
	var nodes []*ConsensusTBFTImpl

	log := tbft_demo.NewLogger()
	// new nodes
	for i := 8081; i <= 8084; i++ {
		node, err := New(i, log)
		if err == nil {
			nodes = append(nodes, node)
		} else {
			panic(err)
		}
	}
	// start nodes
	for _, node := range nodes {
		err := node.Start()
		if err != nil {
			panic(err)
		}
	}

	time.Sleep(60 * time.Second)
	for _, node := range nodes {
		err := node.Stop()
		if err != nil {
			panic(err)
		}
	}
}
