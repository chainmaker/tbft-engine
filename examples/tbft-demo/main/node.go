package main

import (
	"context"
	"crypto/ecdsa"
	"errors"
	"tbft-engine/examples/tbft-demo"

	tbftengine "chainmaker.org/chainmaker/tbft-engine"
)

var (
	defaultChanCap = 1000
	chainId        = "chain1"
	nodes          = map[int]string{
		8081: "127.0.0.1:8081",
		8082: "127.0.0.1:8082",
		8083: "127.0.0.1:8083",
		8084: "127.0.0.1:8084",
	}
	nodeIdEcdsaKeyPair  = make(map[string]*tbft_demo.EcdsaKeyPair)
	validatorsPublicKey = make(map[string]*ecdsa.PublicKey)
)

func init() {
	for _, node := range nodes {
		privateKey, publicKey, _ := tbft_demo.GenKeyPair()
		ecdsaKeyPair := tbft_demo.NewEcdsaKeyPair(privateKey, publicKey)
		nodeIdEcdsaKeyPair[node] = ecdsaKeyPair
		validatorsPublicKey[node] = ecdsaKeyPair.PubKey
	}
}

type ConsensusTBFTImpl struct {
	Id             string
	logger         *tbft_demo.Logger
	closeC         chan struct{}
	handlerManager *tbft_demo.HandlerManager
	engine         *tbftengine.ConsensusTBFTImpl
}

func New(port int, log *tbft_demo.Logger) (*ConsensusTBFTImpl, error) {
	_, ok := nodes[port]
	if !ok {
		return nil, errors.New("invalid port")
	}
	var err error
	consensus := &ConsensusTBFTImpl{}
	var currentHeight uint64 = 0

	var validators []string
	for _, v := range nodes {
		validators = append(validators, v)
	}

	consensus.Id = nodes[port]
	consensus.logger = log
	consensus.closeC = make(chan struct{})
	node, err := tbftengine.NewNode(consensus.logger, consensus.Id, chainId, currentHeight, validators)
	if err != nil {
		return nil, err
	}
	size := defaultChanCap

	consensus.handlerManager = tbft_demo.NewHandlerManager(consensus.logger, node,
		tbft_demo.NewNetHandler(consensus.logger, size, consensus.Id, node),
		tbft_demo.NewBatchMsgInterpreter(size, consensus.Id),
		tbft_demo.NewVerifier(consensus.logger, nodeIdEcdsaKeyPair[nodes[port]], validatorsPublicKey),
		tbft_demo.NewWalHandler(),
		tbft_demo.NewCommitter(consensus.logger, size),
		tbft_demo.NewParamsHandler(),
		tbft_demo.NewProposalHandler(consensus.logger),
	)
	err = consensus.handlerManager.StartrHandler()
	if err != nil {
		consensus.logger.Errorf("New ConsensusTBFTImpl failed, err = %v", err)
		return nil, err
	}
	consensus.engine = node
	return consensus, nil
}

// Start starts the tbft instance with tbft-engine
func (consensus *ConsensusTBFTImpl) Start() error {
	ctx := context.TODO()
	go consensus.procProposeState()
	go consensus.procVerifyFailTxs()
	return consensus.engine.Start(ctx)
}

// Stop implements the Stop method of ConsensusEngine interface.
func (consensus *ConsensusTBFTImpl) Stop() error {
	consensus.handlerManager.StopHandler()
	close(consensus.closeC)
	return consensus.engine.Stop()
}

// procProposeState implements the listen ProposeState method of ConsensusEngine interface.
func (consensus *ConsensusTBFTImpl) procProposeState() {
	consensus.logger.Infof("start procProposeState")
	loop := true

	batchMsgInterpreter, ok := consensus.handlerManager.HandlerMap[tbft_demo.BatchMsgInterpreterName].(*tbft_demo.BatchMsgInterpreter)
	if !ok {
		return
	}

	committer, ok := consensus.handlerManager.HandlerMap[tbft_demo.CommitterName].(*tbft_demo.Committer)
	if !ok {
		return
	}

	for loop {
		select {
		case isProposer := <-consensus.engine.ProposeState():
			status := consensus.engine.Status()
			consensus.logger.Infof("[%s](%d/%d/%s) sendProposeState isProposer: %v",
				consensus.Id, status.Sequence, status.Round, status.Step, isProposer)
			if isProposer {
				batchMsgInterpreter.SetSequence(committer.BlockHeight)
			}
		case <-consensus.closeC:
			loop = false
		}
	}
}

// procVerifyFailTxs implements the listen VerifyFailTxs method of ConsensusEngine interface.
func (consensus *ConsensusTBFTImpl) procVerifyFailTxs() {
	consensus.logger.Infof("start procVerifyFailTxs")
	loop := true
	for loop {
		select {
		case rollBack := <-consensus.engine.RollBack():
			status := consensus.engine.Status()
			consensus.logger.Infof("[%s](%d/%d/%s) delete invalidTxs [%v]",
				consensus.Id, status.Sequence, status.Round, status.Step, rollBack.VerifyFailTxs)
		case <-consensus.closeC:
			loop = false
		}
	}
}
