package tbft_demo

import (
	"encoding/json"
	"errors"
	"fmt"
	tbftengine "chainmaker.org/chainmaker/tbft-engine"
)

type Coder struct {
	logger *Logger
}

// coder
var _ tbftengine.Coder = &Coder{}

func NewProposalHandler(logger *Logger) *Coder {
	return &Coder{
		logger: logger,
	}
}

func (p *Coder) Name() string {
	return ProposalHandlerName
}

func (p *Coder) Close() {
}

type Proposal struct {
	Voter  string
	Height uint64
	Round  int32
	Block  *BatchMsg
	Sign   []byte
}

// 实现Proposal的marshal
func (p *Coder) MarshalProposal(proposal *tbftengine.Proposal) ([]byte, error) {
	bm, ok := proposal.Content.(*BatchMsg)
	if !ok {
		return nil, errors.New("invalid BatchMsg")
	}

	sign, ok := proposal.Signature.([]byte)
	if !ok {
		return nil, errors.New("invalid Signature")
	}

	tbftpbProposal := &Proposal{
		Voter:  proposal.Proposer,
		Height: proposal.Sequence,
		Round:  proposal.Round,
		Block:  bm,
		Sign:   sign,
	}

	data, err := json.Marshal(tbftpbProposal)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("json.Marshal err= %v \n", err))
	}
	return data, nil
}

// 实现Proposal的Unmarshal
func (p *Coder) UnmarshalProposal(b []byte) (*tbftengine.Proposal, error) {
	proposal := &Proposal{}
	err := json.Unmarshal(b, proposal)
	if err != nil {
		p.logger.Infof("MustUnmarshalProposa failed , err = %v", err)
		return nil, err
	}

	return &tbftengine.Proposal{
		Proposer:  proposal.Voter,
		Sequence:  proposal.Height,
		Round:     proposal.Round,
		Key:       proposal.Block.Key(),
		Content:   proposal.Block,
		Signature: proposal.Sign,
	}, nil
}
