package tbft_demo

import (
	"crypto/ecdsa"
	"errors"
	"fmt"

	"encoding/json"
	tbftengine "chainmaker.org/chainmaker/tbft-engine"
	enginePb "chainmaker.org/chainmaker/tbft-engine/pb"
	"github.com/gogo/protobuf/proto"
)

// mustMarshal marshals protobuf message to byte slice or panic when marshal twice both failed
func mustMarshal(msg proto.Message) (data []byte) {
	var err error
	defer func() {
		// while first marshal failed, retry marshal again
		if recover() != nil {
			data, err = proto.Marshal(msg)
			if err != nil {
				panic(err)
			}
		}
	}()

	data, err = proto.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return
}

// mustUnmarshal unmarshals from byte slice to protobuf message or panic
func mustUnmarshal(b []byte, msg proto.Message) {
	if err := proto.Unmarshal(b, msg); err != nil {
		panic(err)
	}
}

type Verifier struct {
	logger              *Logger
	ecdsaKeyPair        *EcdsaKeyPair
	validatorsPublicKey map[string]*ecdsa.PublicKey
}

var _ tbftengine.Verifier = &Verifier{}

func (v *Verifier) Name() string {
	return VerifierName
}

func (v *Verifier) Close() {
}

func NewVerifier(logger *Logger, ecdsaKeyPair *EcdsaKeyPair, validatorsPublicKey map[string]*ecdsa.PublicKey) *Verifier {
	v := &Verifier{
		ecdsaKeyPair:        ecdsaKeyPair,
		validatorsPublicKey: validatorsPublicKey,
		logger:              logger,
	}
	return v
}

func (v *Verifier) AddValidator(validatorPublicKey *ecdsa.PublicKey, nodeId string) error {
	v.validatorsPublicKey[nodeId] = validatorPublicKey
	return nil
}

func (v *Verifier) DeleteValidator(nodeId string) error {
	delete(v.validatorsPublicKey, nodeId)
	return nil
}

func engineProposalToProposal(p *tbftengine.Proposal) (*Proposal, error) {
	if p == nil {
		return nil, errors.New("invalid Proposal")
	}

	bm, ok := p.Content.(*BatchMsg)
	if !ok {
		return nil, errors.New("invalid BatchMsg")
	}

	return &Proposal{
		Voter:  p.Proposer,
		Height: p.Sequence,
		Round:  p.Round,
		Block:  bm,
	}, nil
}

// SignProposal 对Proposal签名
func (v *Verifier) SignProposal(proposal *tbftengine.Proposal) (interface{}, error) {
	tbftpbProposal, err := engineProposalToProposal(proposal)
	if err != nil {
		return nil, err
	}
	proposalBz, err := json.Marshal(tbftpbProposal)
	if err != nil {
		return nil, err
	}

	sign, err := v.ecdsaKeyPair.Sign(proposalBz)
	if err != nil {
		return nil, err
	}

	proposal.Signature = sign
	return sign, nil
}

// SignBatchMsg 对BatchMsg签名
func (v *Verifier) SignBatchMsg(batchMsg tbftengine.BatchMsg) (interface{}, error) {
	bm := batchMsg.(*BatchMsg)

	bmSign := &BatchMsg{
		Proposer: bm.Proposer,
		Index:    bm.Index,
		Hash:     bm.Hash,
		Data:     bm.Data,
	}

	batchMsgBz, err := json.Marshal(bmSign)
	if err != nil {
		return nil, err
	}

	sign, err := v.ecdsaKeyPair.Sign(batchMsgBz)
	if err != nil {
		return nil, err
	}

	bm.Sign = sign
	return sign, nil
}

// SignVote 对vote签名
func (v *Verifier) SignVote(vote *enginePb.Vote) ([]byte, error) {
	voteBz := mustMarshal(vote)

	sign, err := v.ecdsaKeyPair.Sign(voteBz)
	if err != nil {
		return nil, err
	}

	vote.Signature = sign
	return sign, nil
}

// VerifyProposal 验证Proposal
func (v *Verifier) VerifyProposal(proposal *tbftengine.Proposal) error {
	tbftpbProposal, err := engineProposalToProposal(proposal)
	proposalBz, err := json.Marshal(tbftpbProposal)
	if err != nil {
		return err
	}

	publicKey, ok := v.validatorsPublicKey[proposal.Proposer]
	if !ok {
		return errors.New("invalid voter")
	}

	if !v.ecdsaKeyPair.VerifySign(proposalBz, proposal.Signature.([]byte), publicKey) {
		return errors.New("invalid Signature")
	}

	return nil
}

// VerifyBatchMsg 验证BatchMsg
func (v *Verifier) VerifyBatchMsg(batchMsg tbftengine.BatchMsg) (*tbftengine.VerifyResult, error) {
	bm := batchMsg.(*BatchMsg)
	bmSign := &BatchMsg{
		Proposer: bm.Proposer,
		Index:    bm.Index,
		Hash:     bm.Hash,
		Data:     bm.Data,
	}
	batchMsgBz, err := json.Marshal(bmSign)
	if err != nil {
		return nil, err
	}

	publicKey, ok := v.validatorsPublicKey[bm.Proposer]
	if !ok {
		return nil, errors.New("invalid proposer")
	}
	if !v.ecdsaKeyPair.VerifySign(batchMsgBz, bm.Sign, publicKey) {
		return nil, errors.New("invalid Signature")
	}

	return &tbftengine.VerifyResult{
		VerifiedBatchMsg: bm,
		Code:             enginePb.VerifyResult_Code_VerifyResult_SUCCESS,
		Msg:              "verifyBatchMsg succeed",
	}, nil
}

// VerifyVote 验证vote
func (v *Verifier) VerifyVote(voteProto *enginePb.Vote) error {
	voteProtoCopy := proto.Clone(voteProto)
	vote, ok := voteProtoCopy.(*enginePb.Vote)
	if !ok {
		return fmt.Errorf("interface to *tbftpb.Vote failed")
	}
	vote.Signature = nil
	message := mustMarshal(vote)

	publicKey, ok := v.validatorsPublicKey[voteProto.Voter]
	if !ok {
		return errors.New("invalid Voter")
	}

	if !v.ecdsaKeyPair.VerifySign(message, voteProto.Signature, publicKey) {
		return errors.New("invalid Signature")
	}

	return nil
}

// VerifyBlockWithRwSets proc VerifyBlockWithRwSets by core
func (v *Verifier) VerifyBlockWithRwSets(batchMsg tbftengine.BatchMsg, txsRwSet interface{},
	vs *enginePb.VoteSet) error {
	return nil
}