/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package tbft_engine

import (
	"sync"

	tbftpb "chainmaker.org/chainmaker/tbft-engine/pb"
)

// ConsensusState represents the consensus state of the node
type ConsensusState struct {
	logger Logger
	// node id
	Id string
	// current height
	Height uint64
	// current round
	Round int32
	// current step
	Step tbftpb.Step

	// proposal
	Proposal *TBFTProposal
	// verifing proposal
	VerifingProposal *TBFTProposal
	LockedRound      int32
	// locked proposal
	LockedProposal *Proposal
	ValidRound     int32
	// valid proposal
	ValidProposal      *Proposal
	heightRoundVoteSet *heightRoundVoteSet
}

//
// ConsensusFutureMsg
// @Description: represents the consensus msg of future
//
type ConsensusFutureMsg struct {
	Proposal           map[int32]*Proposal
	heightRoundVoteSet *heightRoundVoteSet
}

//
// ConsensusFutureMsgCache
// @Description: Cache future consensus msg
//
type ConsensusFutureMsgCache struct {
	logger Logger
	sync.Mutex
	size            uint64
	consensusHeight uint64
	cache           map[uint64]*ConsensusFutureMsg
}

//
// ConsensusFutureMsgCache
// @Description: Create a new future Msg cache
// @param size
// @return *ConsensusFutureMsgCache
//
func newConsensusFutureMsgCache(logger Logger, size, height uint64) *ConsensusFutureMsgCache {
	return &ConsensusFutureMsgCache{
		logger:          logger,
		size:            size,
		consensusHeight: height,
		cache:           make(map[uint64]*ConsensusFutureMsg, size),
	}
}

// NewConsensusFutureMsg creates a new future msg instance
func NewConsensusFutureMsg(logger Logger, height uint64, round int32,
	validators *validatorSet) *ConsensusFutureMsg {
	cs := &ConsensusFutureMsg{Proposal: make(map[int32]*Proposal)}
	cs.heightRoundVoteSet = newHeightRoundVoteSet(logger, height, round, validators)

	return cs
}

// updateConsensusHeight update height of consensus
func (cfc *ConsensusFutureMsgCache) updateConsensusHeight(height uint64) {
	cfc.Lock()
	defer cfc.Unlock()
	cfc.consensusHeight = height

	// delete the cache
	cfc.gc()
}

// addFutureProposal add future proposal in cache
func (cfc *ConsensusFutureMsgCache) addFutureProposal(logger Logger, validators *validatorSet,
	proposal *Proposal) {
	cfc.Lock()
	defer cfc.Unlock()

	// cache future proposal
	// we cahce : consensusHeight <= futureMsg.height <=  consensusHeight + size
	if cfc.consensusHeight+cfc.size < proposal.Sequence || proposal.Sequence < cfc.consensusHeight {
		return
	}
	if _, ok := cfc.cache[proposal.Sequence]; !ok {
		cs := NewConsensusFutureMsg(logger, proposal.Sequence, proposal.Round, validators)
		cfc.cache[proposal.Sequence] = cs
	}

	cfc.cache[proposal.Sequence].Proposal[proposal.Round] = proposal
	cfc.logger.Debugf("addFutureProposal proposal is [%s/%d/%d] %x", proposal.Proposer, proposal.Sequence,
		proposal.Round, proposal.Key)
}

// addFutureVote add future vote in cache
func (cfc *ConsensusFutureMsgCache) addFutureVote(logger Logger, validators *validatorSet,
	vote *tbftpb.Vote) {
	cfc.Lock()
	defer cfc.Unlock()

	// cache future vote
	// we cahce : consensusHeight < futureMsg.height <=  consensusHeight + size
	if cfc.consensusHeight+cfc.size < vote.Sequence || vote.Sequence <= cfc.consensusHeight {
		return
	}
	if _, ok := cfc.cache[vote.Sequence]; !ok {
		cs := NewConsensusFutureMsg(logger, vote.Sequence, vote.Round, validators)
		cfc.cache[vote.Sequence] = cs
	}

	_, err := cfc.cache[vote.Sequence].heightRoundVoteSet.addVote(vote)
	if err != nil {
		cfc.logger.Debugf("addFutureVote addVote %v,  err: %v", vote, err)
	}
}

// getConsensusFutureProposal get future proposal in cache
func (cfc *ConsensusFutureMsgCache) getConsensusFutureProposal(height uint64, round int32) *Proposal {
	cfc.Lock()
	defer cfc.Unlock()

	if state, ok := cfc.cache[height]; ok {
		return state.Proposal[round]
	}

	return nil
}

// getConsensusFutureProposal get future vote in cache
func (cfc *ConsensusFutureMsgCache) getConsensusFutureVote(height uint64, round int32) *roundVoteSet {
	cfc.Lock()
	defer cfc.Unlock()

	if state, ok := cfc.cache[height]; ok {
		return state.heightRoundVoteSet.getRoundVoteSet(round)
	}

	return nil
}

//
// gc
// @Description: Delete the cache before the consensus height
// @receiver cache
// @param height
//
func (cfc *ConsensusFutureMsgCache) gc() {
	// delete every 10 heights
	if cfc.consensusHeight%10 != 0 {
		return
	}

	// delete the cache before the consensus height
	for k := range cfc.cache {
		cfc.logger.Debugf("futureCahce delete ,gc params: %d,%d,%d", k, cfc.size, cfc.consensusHeight)
		if k < cfc.consensusHeight {
			delete(cfc.cache, k)
		}
	}
}

// NewConsensusState creates a new ConsensusState instance
func NewConsensusState(logger Logger, id string) *ConsensusState {
	cs := &ConsensusState{
		logger: logger,
		Id:     id,
	}
	return cs
}

// toProto serializes the ConsensusState instance
func (cs *ConsensusState) toProto() *tbftpb.ConsensusState {
	if cs == nil {
		return nil
	}
	csProto := &tbftpb.ConsensusState{
		Id:     cs.Id,
		Height: cs.Height,
		Round:  cs.Round,
		Step:   cs.Step,
		//Proposal:           cs.Proposal.PbMsg,
		//VerifingProposal:   cs.VerifingProposal.PbMsg,
		HeightRoundVoteSet: cs.heightRoundVoteSet.ToProto(),
	}
	return csProto
}

//
// consensusStateCache
// @Description: Cache historical consensus state
//
type consensusStateCache struct {
	sync.Mutex
	size  uint64
	cache map[uint64]*ConsensusState
}

//
// newConsensusStateCache
// @Description: Create a new state cache
// @param size
// @return *consensusStateCache
//
func newConsensusStateCache(size uint64) *consensusStateCache {
	return &consensusStateCache{
		size:  size,
		cache: make(map[uint64]*ConsensusState, size),
	}
}

//
// addConsensusState
// @Description: Add a new state to the cache
// @receiver cache
// @param state
//
func (cache *consensusStateCache) addConsensusState(state *ConsensusState) {
	if state == nil || state.Height <= 0 {
		return
	}

	cache.Lock()
	defer cache.Unlock()

	cache.cache[state.Height] = state
	cache.gc(state.Height)
}

//
// getConsensusState
// @Description: Get the desired consensus state from the cache, and return nil if it doesn't exist
// @receiver cache
// @param height
// @return *ConsensusState
//
func (cache *consensusStateCache) getConsensusState(height uint64) *ConsensusState {
	cache.Lock()
	defer cache.Unlock()

	if state, ok := cache.cache[height]; ok {
		return state
	}

	return nil
}

//
// gc
// @Description: Delete too many caches, triggered every time a new state is added to the cache
// @receiver cache
// @param height
//
func (cache *consensusStateCache) gc(height uint64) {
	for k := range cache.cache {
		//if k < (height - cache.size) {
		cache.cache[k].logger.Debugf("state delete ,gc params: %d,%d,%d", k, cache.size, height)
		if (k + cache.size) <= height {
			delete(cache.cache, k)
		}
	}
}

type VerifyResult struct {
	VerifiedBatchMsg BatchMsg
	Code             tbftpb.VerifyResult_Code
	Msg              string
	TxsRwSet         interface{}
	VerifyFailTxs    []string
}

type VerifyFailTxs struct {
	Sequence uint64
	TxIds    []string
}

type proposedProposal struct {
	proposedBlock *ProposalBatchMsg
	qc            []*tbftpb.Vote
}

type ProposalBatchMsg struct {
	BatchMsg    BatchMsg
	CutBatchMsg BatchMsg
	TxsRwSet    interface{}
}

// Proposal defined a consesensus proposal which can
// be gossiped to other node and can be serilized
// for persistent store.
type Proposal struct {
	Proposer  string
	Sequence  uint64
	Round     int32
	Content   BatchMsg
	Key       []byte
	Signature interface{}
	TxsRwSet  interface{}
	Qc        []*tbftpb.Vote
}

// WalEntry defined wal types
type WalEntry struct {
	WalEntryType tbftpb.WalEntryType
	Sequence     uint64
	Proposal     *Proposal
}

//
type ConsensusRollBack struct {
	Sequence      uint64
	Proposal      *Proposal
	VerifyFailTxs *VerifyFailTxs
}
