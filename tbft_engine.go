/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package tbft_engine

import (
	"bytes"
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"time"

	tbftpb "chainmaker.org/chainmaker/tbft-engine/pb"

	"reflect"

	"github.com/gogo/protobuf/proto"
	"go.uber.org/zap"
)

var clog = zap.S()

var (
	defaultChanCap = 1000
	nilHash        = []byte("NilHash")
	//consensusStateKey              = []byte("ConsensusStateKey")
	defaultConsensusStateCacheSize  = uint64(10)
	defaultConsensusFutureCacheSize = uint64(10)
)

const (
	// DefaultBlocksPerProposer The number of blocks each proposer can propose
	DefaultBlocksPerProposer = uint64(1)
	// TimeoutPrevote Timeout of waiting for >2/3 prevote
	TimeoutPrevote = 30 * time.Second
	// TimeoutPrevoteDelta Increased time delta of TimeoutPrevote between round
	TimeoutPrevoteDelta = 1 * time.Second
	// TimeoutPrecommit Timeout of waiting for >2/3 precommit
	TimeoutPrecommit = 30 * time.Second
	// TimeoutPrecommitDelta Increased time delta of TimeoutPrecommit between round
	TimeoutPrecommitDelta = 1 * time.Second
	// TimeoutCommit Timeout to wait for precommite
	TimeoutCommit = 30 * time.Second
	// TimeDisconnet the duration of node disconnectio(3000ms)
	TimeDisconnet = 3000
)

var _ TBFTConsensuEngine = &ConsensusTBFTImpl{}

// ConsensusTBFTImpl is the implementation of TBFT algorithm
// and it implements the ConsensusEngine interface.
type ConsensusTBFTImpl struct {
	ctx context.Context
	sync.RWMutex
	logger Logger
	// chain id
	chainID string
	// node id
	Id string
	// start height
	startHeight uint64
	// stop tbft
	closeC chan struct{}
	// walHandler is used to record the consensus state and prevent forks
	walHandler WalHandler
	// netHandler is used for receiving and sending consensus messages
	netHandler NetHandler
	// committer is used for committing batchMsg
	committer Committer
	// paramsHandler is used to get params for new consensus
	paramsHandler ParamsHandler
	// batchMsgInterpreter is used for operating batchMsg
	batchMsgInterpreter BatchMsgInterpreter
	//
	proposalCoder Coder
	// verifier is used for verifying and signing
	verifier Verifier
	// validator Set
	validatorSet *validatorSet
	// Current Consensus State
	*ConsensusState
	// History Consensus State（Save 10）
	// When adding n, delete the cache before n-10
	consensusStateCache *consensusStateCache
	// Cache future consensus msg
	// When update height of consensus, delete the cache before height (triggered every 10 heights)
	consensusFutureMsgCache *ConsensusFutureMsgCache
	// timeScheduler is used by consensus for shecdule timeout events.
	// Outdated timeouts will be ignored in processing.
	timeScheduler *timeScheduler
	// The Proposer of the last block commit height
	lastHeightProposer string
	// channel for processing a block
	proposedBlockC chan *proposedProposal
	// channel for waiting to verify batchMsg
	verifyResultC chan *VerifyResult
	// channel for notifying the user of the completion of the commit
	CommitFinishC chan interface{}
	// channel used to enter new height
	blockHeightC chan uint64
	// channel used to externalMsg（msgbus）
	externalMsgC chan *ConsensusMsg
	// Use in handleConsensusMsg method
	internalMsgC chan *ConsensusMsg
	// send propose state
	proposeState chan bool
	// send verify fail txs
	verifyFailTxs *VerifyFailTxs
	// consensus  roll back
	rollBackC chan *ConsensusRollBack

	invalidTxs []string

	// Timeout = TimeoutPropose + TimeoutProposeDelta * round
	TimeoutPropose        time.Duration
	TimeoutProposeDelta   time.Duration
	TimeoutProposeOptimal time.Duration
	ProposeOptimal        bool
	ProposeOptimalTimer   *time.Timer

	// The specific time points of each stage of each round in each altitude
	// for printing logs
	metrics *heightMetrics
	// Tbft Consistent Engine
	// will only work if the node is abnormal
	// prevent message loss
	consistentEngine *StatusConsistentEngine
}

//
// Type
// @Description: return Status Type（ConsistentEngine）
// @receiver consensus
// @return int
//
func (consensus *ConsensusTBFTImpl) Type() int8 {
	return TypeLocalTBFTState
}

//
// Data
// @Description: return Status Data（ConsistentEngine）
// @receiver consensus
// @return interface{}
//
func (consensus *ConsensusTBFTImpl) Data() interface{} {
	return consensus
}

//
// Update
// @Description: Update state
// @receiver consensus
// @param state
//
func (consensus *ConsensusTBFTImpl) Update(state Status) {
	consensus.logger.Debugf("update %s", state.Type())
}

// mustMarshal marshals protobuf message to byte slice or panic when marshal twice both failed
func mustMarshal(msg proto.Message) (data []byte) {
	var err error
	defer func() {
		// while first marshal failed, retry marshal again
		if recover() != nil {
			data, err = proto.Marshal(msg)
			if err != nil {
				panic(err)
			}
		}
	}()

	data, err = proto.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return
}

// mustUnmarshal unmarshals from byte slice to protobuf message or panic
func mustUnmarshal(b []byte, msg proto.Message) {
	if err := proto.Unmarshal(b, msg); err != nil {
		panic(err)
	}
}

// NewNode creates a tbft consensus instance
func NewNode(logger Logger, id, chainId string, height uint64, validators []string) (*ConsensusTBFTImpl, error) {
	consensus := &ConsensusTBFTImpl{}
	consensus.logger = logger
	consensus.logger.Infof("New ConsensusTBFTImpl[%s]", chainId)
	consensus.chainID = chainId
	consensus.Id = id
	consensus.startHeight = height
	consensus.closeC = make(chan struct{})

	consensus.proposedBlockC = make(chan *proposedProposal, defaultChanCap)
	consensus.verifyResultC = make(chan *VerifyResult, defaultChanCap)
	consensus.CommitFinishC = make(chan interface{}, defaultChanCap)
	consensus.blockHeightC = make(chan uint64, defaultChanCap)
	consensus.externalMsgC = make(chan *ConsensusMsg, defaultChanCap)
	consensus.internalMsgC = make(chan *ConsensusMsg, defaultChanCap)
	consensus.proposeState = make(chan bool, 1)
	consensus.rollBackC = make(chan *ConsensusRollBack, 1)

	consensus.validatorSet = NewValidatorSet(consensus.logger, validators, DefaultBlocksPerProposer)
	consensus.ConsensusState = NewConsensusState(consensus.logger, consensus.Id)
	consensus.consensusStateCache = newConsensusStateCache(defaultConsensusStateCacheSize)
	consensus.consensusFutureMsgCache = newConsensusFutureMsgCache(consensus.logger,
		defaultConsensusFutureCacheSize, 0)
	consensus.timeScheduler = newTimeSheduler(consensus.logger, id)
	consensus.ProposeOptimal = false
	consensus.ProposeOptimalTimer = time.NewTimer(0 * time.Second)
	consensus.ProposeOptimalTimer.Stop()

	return consensus, nil
}

func IsNil(i interface{}) bool {
	if i == nil {
		return true
	}
	if reflect.TypeOf(i) == nil {
		return true
	}
	vi := reflect.ValueOf(i)
	if vi.Kind() == reflect.Ptr {
		return vi.IsNil()
	}
	return false
}

func (consensus *ConsensusTBFTImpl) RegisterNetHandler(netHandler NetHandler) error {
	if !IsNil(netHandler) {
		consensus.netHandler = netHandler
	}
	return nil
}

func (consensus *ConsensusTBFTImpl) RegisterInterpreter(batchMsgInterpreter BatchMsgInterpreter) error {
	if !IsNil(batchMsgInterpreter) {
		consensus.batchMsgInterpreter = batchMsgInterpreter
	}
	return nil
}

func (consensus *ConsensusTBFTImpl) RegisterVerifyHandler(verifier Verifier) error {
	if !IsNil(verifier) {
		consensus.verifier = verifier
	}
	return nil
}

func (consensus *ConsensusTBFTImpl) RegisterWalHandler(walHandler WalHandler) error {
	if !IsNil(walHandler) {
		consensus.walHandler = walHandler
	}
	return nil
}

func (consensus *ConsensusTBFTImpl) RegisterCommitter(committer Committer) error {
	if !IsNil(committer) {
		consensus.committer = committer
	}
	return nil
}

func (consensus *ConsensusTBFTImpl) RegisterLogger(logger Logger) error {
	if !IsNil(logger) {
		consensus.logger = logger
	}
	return nil
}

func (consensus *ConsensusTBFTImpl) RegisterParamsHandler(paramsHandler ParamsHandler) error {
	if !IsNil(paramsHandler) {
		consensus.paramsHandler = paramsHandler
	}
	return nil
}

func (consensus *ConsensusTBFTImpl) RegisterProposalCoder(proposalHandler Coder) error {
	if !IsNil(proposalHandler) {
		consensus.proposalCoder = proposalHandler
	}
	return nil
}

func (consensus *ConsensusTBFTImpl) RollBack() <-chan *ConsensusRollBack {
	return consensus.rollBackC
}

func (consensus *ConsensusTBFTImpl) ProposeState() <-chan bool {
	return consensus.proposeState
}

func (consensus *ConsensusTBFTImpl) Status() *tbftpb.Status {
	return &tbftpb.Status{
		Sequence: consensus.Height,
		Round:    consensus.Round,
		Step:     consensus.Step,
	}
}

//
// StartConsistentEngine
// @Description: Start ConsistentEngine
// @receiver consensus
//
func (consensus *ConsensusTBFTImpl) StartConsistentEngine() {

	status := make(map[int8]Status)
	status[consensus.Type()] = consensus
	localStatus := &Node{
		id:     consensus.Id,
		status: status,
	}
	message := NewTbftConsistentMessage(consensus.logger, consensus.netHandler)
	// create consistentEngine
	consensus.consistentEngine = NewConsistentService(
		localStatus,
		message,
		consensus.logger)

	tbftStatusInterceptor := &StatusInterceptor{}
	// register a tbftStatusInterceptor
	err := consensus.consistentEngine.RegisterStatusInterceptor(InterceptorTbft, tbftStatusInterceptor)
	if err != nil {
		consensus.logger.Errorf(err.Error())
	}
	tbftDecoder := &StatusDecoder{log: consensus.logger,
		tbftImpl: consensus}
	// register a tbftDecoder
	err = consensus.consistentEngine.RegisterStatusCoder(tbftDecoder)
	if err != nil {
		consensus.logger.Errorf("RegisterStatusCoder err:%s", err)
		return
	}
	// add a remoter
	for _, id := range consensus.validatorSet.Validators {
		// local node do not need to be added to the consistency engine
		if id == consensus.Id {
			continue
		}
		remoteState := &RemoteState{Id: id, Height: 0}
		status := make(map[int8]Status)
		status[remoteState.Type()] = remoteState
		remoteNodeStatus := &Node{id: id, status: status}
		err = consensus.consistentEngine.PutRemoter(id, remoteNodeStatus)
		if err != nil {
			consensus.logger.Errorf(err.Error())
		}
	}
	// create a tbftStatusBroadcaster
	tbftStatusBroadcaster := NewTBFTStatusBroadcaster(
		consensus.logger)
	// add a tbftStatusBroadcaster to the consistentEngine
	err = consensus.consistentEngine.AddBroadcaster(StatusBroadcasterTbft, tbftStatusBroadcaster)
	if err != nil {
		consensus.logger.Errorf("AddBroadcaster err:%s", err)
	}
	// start the consistentEngine
	ctx := context.TODO()
	err = consensus.consistentEngine.Start(ctx)
	if err != nil {
		consensus.logger.Errorf(err.Error())
	}
}

// Start starts the tbft instance with:
// 1. Register to message bus for subscribing topics
// 2. Start background goroutinues for processing events
// 3. Start timeScheduler for processing timeout shedule
func (consensus *ConsensusTBFTImpl) Start(ctx context.Context) error {
	if ctx == nil || ctx.Err() != nil {
		return errors.New("invalid context")
	}
	if consensus.netHandler == nil || consensus.verifier == nil || consensus.batchMsgInterpreter == nil ||
		consensus.committer == nil || consensus.paramsHandler == nil || consensus.proposalCoder == nil {
		return errors.New("all handler need to be registered")
	}

	consensus.ctx = ctx
	consensus.logger.Infof("start ConsensusTBFTImpl[%s]", consensus.Id)
	consensus.timeScheduler.Start()
	consensus.StartConsistentEngine()

	err := consensus.replayWal()
	if err != nil {
		return err
	}

	go consensus.start()
	go consensus.handle()
	return nil
}

//
// sendProposeState
// @Description: Send propose state via msgbus
// @receiver consensus
// @param isProposer Is it a proposal node
//
func (consensus *ConsensusTBFTImpl) sendProposeState(isProposer bool) {
	consensus.logger.Infof("[%s](%d/%d/%s) sendProposeState isProposer: %v",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, isProposer)
	consensus.proposeState <- isProposer
}

// Stop implements the Stop method of ConsensusEngine interface.
func (consensus *ConsensusTBFTImpl) Stop() error {
	consensus.Lock()
	defer consensus.Unlock()

	consensus.logger.Infof("[%s](%d/%d/%s) stopped", consensus.Id, consensus.Height, consensus.Round,
		consensus.Step)
	ctx := context.TODO()
	err := consensus.consistentEngine.Stop(ctx)
	if err != nil {
		consensus.logger.Errorf(err.Error())
	}
	close(consensus.closeC)
	return nil
}

// 1. when leadership transfer, change consensus state and send singal
// atomic.StoreInt32()
// proposable <- atomic.LoadInt32(consensus.isLeader)

// 2. when receive pre-prepare block, send block to verifyBlockC
// verifyBlockC <- block

// 3. when receive commit block, send block to commitBlockC
// commitBlockC <- block
func (consensus *ConsensusTBFTImpl) start() {
	consensus.logger.Debugf("[%s] start : %s", consensus.Id)

	for {
		select {
		case proposedBlock := <-consensus.batchMsgInterpreter.PrepareBatchMsg():
			consensus.proposedBlockC <- &proposedProposal{proposedBlock, nil}
		case consensusMsg := <-consensus.netHandler.Listen():
			go func(msg interface{}) {
				if msg, ok := msg.([]byte); ok {
					if consensusMsg := consensus.createConsensusMsgFromTBFTMsgBz(msg); consensusMsg != nil {
						if consensusMsg.Type == tbftpb.TBFTMsgType_MSG_STATE {
							consensus.consistentEngine.msg.AddNetMsg(consensusMsg.Msg)
						} else {
							consensus.externalMsgC <- consensusMsg
						}
					} else {
						consensus.logger.Warnf("assert Consensus Msg failed")
					}
				} else {
					consensus.logger.Warnf("assert NetMsg failed")
				}
			}(consensusMsg)
		case height := <-consensus.committer.CommitDone():
			consensus.blockHeightC <- height
		}
	}
}

//
// updateChainConfig
// @Description: Update configuration, return validators to be added and removed
// @receiver consensus
// @return addedValidators
// @return removedValidators
// @return err
//
func (consensus *ConsensusTBFTImpl) updateChainConfig() (addedValidators []string, removedValidators []string,
	err error) {
	consensus.logger.Debugf("[%s](%d/%d/%v) update chain config",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step)

	validators, timeoutPropose, timeoutProposeDelta, tbftBlocksPerProposer, proposeOptimal,
		timeoutProposeOptimal, err := consensus.paramsHandler.GetNewParams()
	if err != nil {
		return nil, nil, err
	}

	consensus.ProposeOptimal = proposeOptimal
	consensus.TimeoutProposeOptimal = timeoutProposeOptimal
	consensus.TimeoutPropose = timeoutPropose
	consensus.TimeoutProposeDelta = timeoutProposeDelta
	consensus.logger.Debugf("[%s](%d/%d/%v) update chain config : "+
		" TimeoutPropose: %v, TimeoutProposeDelta: %v, proposeOptimal: %v"+
		"TimeoutProposeOptimal: %v, validators: %v",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		consensus.TimeoutPropose, consensus.TimeoutProposeDelta, consensus.ProposeOptimal,
		consensus.TimeoutProposeOptimal, validators)
	err = consensus.validatorSet.updateBlocksPerProposer(tbftBlocksPerProposer)
	if err != nil {
		consensus.logger.Errorf("update Proposer per Blocks failed err: %s", err)
	}

	return consensus.validatorSet.updateValidators(validators)
}

//
// extractProposeTimeout
// @Description: extract the timeout based on value
// @receiver consensus
// @param value
// @return timeoutPropose
// @return err
//
func (consensus *ConsensusTBFTImpl) extractProposeTimeout(value string) (timeoutPropose time.Duration, err error) {
	if timeoutPropose, err = time.ParseDuration(value); err != nil {
		consensus.logger.Infof("[%s](%d/%d/%v) update chain config, TimeoutPropose: %v,"+
			" TimeoutProposeDelta: %v,"+" parse TimeoutPropose error: %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			consensus.TimeoutPropose, consensus.TimeoutProposeDelta, err)
	}
	return
}

//
// extractProposeTimeoutDelta
// @Description: extract the delta timeout based on value
// @receiver consensus
// @param value
// @return timeoutProposeDelta
// @return err
//
func (consensus *ConsensusTBFTImpl) extractProposeTimeoutDelta(value string) (timeoutProposeDelta time.Duration,
	err error) {
	if timeoutProposeDelta, err = time.ParseDuration(value); err != nil {
		consensus.logger.Infof("[%s](%d/%d/%v) update chain config, TimeoutPropose: %v,"+
			" TimeoutProposeDelta: %v,"+" parse TimeoutProposeDelta error: %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			consensus.TimeoutPropose, consensus.TimeoutProposeDelta, err)
	}
	return
}

//
// extractBlocksPerProposer
// @Description: Calculate the number of extracted blocks for each proposer
// @receiver consensus
// @param value
// @return tbftBlocksPerProposer
// @return err
//
func (consensus *ConsensusTBFTImpl) extractBlocksPerProposer(value string) (tbftBlocksPerProposer uint64, err error) {
	if tbftBlocksPerProposer, err = strconv.ParseUint(value, 10, 32); err != nil {
		consensus.logger.Infof("[%s](%d/%d/%v) update chain config, parse BlocksPerProposer error: %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, err)
		return
	}
	if tbftBlocksPerProposer <= 0 {
		err = fmt.Errorf("invalid TBFT_blocks_per_proposer: %d", tbftBlocksPerProposer)
		return
	}
	return
}

//
// handle
// @Description: Main methods of consensus process
// @receiver consensus
//
func (consensus *ConsensusTBFTImpl) handle() {
	consensus.logger.Infof("[%s] handle start", consensus.Id)
	defer consensus.logger.Infof("[%s] handle end", consensus.Id)

	loop := true
	for loop {
		select {
		case proposal := <-consensus.proposedBlockC:
			consensus.handleProposedBlock(proposal)
		case result := <-consensus.verifyResultC:
			consensus.handleVerifyResult(result)
		case height := <-consensus.blockHeightC:
			consensus.handleBlockHeight(height)
		case msg := <-consensus.externalMsgC:
			consensus.logger.Debugf("[%s] receive from externalMsgC %s", consensus.Id, msg.Type)
			consensus.handleConsensusMsg(msg)
		case msg := <-consensus.internalMsgC:
			consensus.logger.Debugf("[%s] receive from internalMsgC %s", consensus.Id, msg.Type)
			consensus.handleConsensusMsg(msg)
		case ti := <-consensus.timeScheduler.GetTimeoutC():
			consensus.handleTimeout(ti)
		case <-consensus.ProposeOptimalTimer.C:
			consensus.handleProposeOptimalTimeout()
		case <-consensus.closeC:
			loop = false
		}
	}
}

//
// handleProposedBlock
// @Description: Process the proposed block
// @receiver consensus
// @param proposedBlock
//
func (consensus *ConsensusTBFTImpl) handleProposedBlock(proposedBatchMsg *proposedProposal) {
	consensus.Lock()
	defer consensus.Unlock()

	receiveBlockTime := CurrentTimeMillisSeconds()
	batchMsg := proposedBatchMsg.proposedBlock.BatchMsg
	consensus.logger.Infof("[%s](%d/%d/%s) receive block from core engine (%d/%x), isProposer: %v",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		batchMsg.Sequence(), batchMsg.Key(), consensus.isProposer(consensus.Height,
			consensus.Round),
	)

	// process only blocks of current height
	if batchMsg.Sequence() != consensus.Height {
		consensus.logger.Warnf("[%s](%d/%d/%v) handle proposed block failed,"+
			" receive block from invalid height: %d",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, batchMsg.Sequence())
		return
	}

	// only process blocks for which you are the proposer
	if !consensus.isProposer(consensus.Height, consensus.Round) {
		consensus.logger.Warnf("[%s](%d/%d/%s) receive proposal from core engine (%d/%x), but isProposer: %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			batchMsg.Sequence(), batchMsg.Key(), consensus.isProposer(consensus.Height, consensus.Round),
		)
		return
	}
	// continue processing only if you are in the proposal stage（Step_PROPOSE）
	if consensus.Step != tbftpb.Step_PROPOSE {
		consensus.logger.Warnf("[%s](%d/%d/%s) receive proposal from core engine (%d/%x), step error",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			batchMsg.Sequence(), batchMsg.Key(),
		)
		return
	}

	// Tell the core engine that it is no longer a proposal node and does not propose blocks anymore
	consensus.sendProposeState(false)

	proposal := NewProposal(consensus.Id, consensus.Height, consensus.Round, batchMsg.Key(), batchMsg)
	// this is not a locked proposal
	if proposedBatchMsg.qc == nil {
		// Add hash and signature to the block of consensus, actually also add them to the block of core
		sign, err := consensus.verifier.SignBatchMsg(batchMsg)
		if err != nil {
			consensus.logger.Errorf("[%s]sign block failed, %s", consensus.Id, err)
			return
		}
		proposal.Key = batchMsg.Key()
		batchMsg.SetSignature(sign)
	} else {
		// the locked proposal need txrwset and qc to send other nodes
		proposal.TxsRwSet = proposedBatchMsg.proposedBlock.TxsRwSet
		proposal.Qc = proposedBatchMsg.qc
	}
	signBlockTime := CurrentTimeMillisSeconds()

	// proposal for cut batchMsg
	if !IsNil(proposedBatchMsg.proposedBlock.CutBatchMsg) {
		oriBatchMsg := proposedBatchMsg.proposedBlock.BatchMsg
		proposal.Content = proposedBatchMsg.proposedBlock.CutBatchMsg
		sign, err := consensus.verifier.SignProposal(proposal)
		if err != nil {
			consensus.logger.Errorf("sign proposal err %s", err)
			return
		}
		proposal.Signature = sign
		consensus.Proposal = consensus.NewTBFTProposal(proposal, true)
		proposal.Content = oriBatchMsg
	} else {
		sign, err := consensus.verifier.SignProposal(proposal)
		if err != nil {
			consensus.logger.Errorf("sign proposal err %s", err)
			return
		}
		proposal.Signature = sign
		consensus.Proposal = consensus.NewTBFTProposal(proposal, true)
	}
	// need txsRWSet to saved in wal
	consensus.Proposal.PbMsg.TxsRwSet = proposedBatchMsg.proposedBlock.TxsRwSet
	signProposalTime := CurrentTimeMillisSeconds()

	consensus.logger.Infof("[%s](%d/%d/%s) generated proposal (%d/%d/%x), "+
		"time:[signBlock:%dms,signProposal:%dms,total:%dms]",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		proposal.Sequence, proposal.Round, proposal.Key,
		signBlockTime-receiveBlockTime, signProposalTime-signBlockTime, signProposalTime-receiveBlockTime)

	// send proposal
	msg := createProposalTBFTMsg(consensus.Proposal)
	payload := mustMarshal(msg)
	_ = consensus.netHandler.BroadCastNetMsg(payload, "")
	// Enter the prevote stage
	consensus.enterPrevote(consensus.Height, consensus.Round)
}

//
// handleVerifyResult
// @Description: Process validation results
// @receiver consensus
// @param verifyResult
//
func (consensus *ConsensusTBFTImpl) handleVerifyResult(verifyResult *VerifyResult) {
	consensus.Lock()
	defer consensus.Unlock()

	receiveResTime := CurrentTimeMillisSeconds()

	height := verifyResult.VerifiedBatchMsg.Sequence()
	hash := verifyResult.VerifiedBatchMsg.Key()
	consensus.logger.Infof("[%s](%d/%d/%s) receive verify result (%d/%x) %v",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		height, hash, verifyResult.Code)

	if consensus.VerifingProposal == nil {
		consensus.logger.Warnf("[%s](%d/%d/%s) receive verify result failed, (%d/%x) %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			height, hash, verifyResult.Code,
		)
		return
	}

	if consensus.Height == height &&
		consensus.Round == consensus.VerifingProposal.PbMsg.Round &&
		verifyResult.Code == tbftpb.VerifyResult_Code_VerifyResult_FAIL {
		consensus.logger.Warnf("[%s](%d/%d/%s) %x receive verify result (%d/%x) %v failed",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			consensus.VerifingProposal.PbMsg.Key,
			height, hash, verifyResult.Code,
		)
		consensus.VerifingProposal = nil

		// inconsistent RwSet
		if verifyResult.VerifyFailTxs != nil && len(verifyResult.VerifyFailTxs) > 0 {
			consensus.logger.Infof("handleVerifyResult failed , invalidTx = %v", verifyResult.VerifyFailTxs)
			consensus.invalidTxs = verifyResult.VerifyFailTxs
			// inconsistent RWSets enter prevote immediately
			consensus.enterPrevote(consensus.Height, consensus.Round)
		}
		return
	}

	// if there are quorum pre_commit vote, then commit block
	if bytes.Equal(consensus.VerifingProposal.PbMsg.Key, hash) {
		if consensus.heightRoundVoteSet != nil && consensus.heightRoundVoteSet.precommits(consensus.Round) != nil {
			voteSet := consensus.heightRoundVoteSet.precommits(consensus.Round)
			quorumHash, ok := voteSet.twoThirdsMajority()
			//if ok && bytes.Compare(quorumHash, consensus.VerifingProposal.Block.Header.BlockHash) == 0 {
			if ok && bytes.Equal(quorumHash, consensus.VerifingProposal.PbMsg.Key) {
				consensus.Proposal = consensus.VerifingProposal

				// Commit block to core engine
				consensus.commitBlock(consensus.Proposal.PbMsg.Content, voteSet.ToProto())
				return
			}
		}
	}

	// maybe received multiple proposal, the last proposal verify succeed
	// the previous invalid txs is no longer needed
	consensus.invalidTxs = nil

	if consensus.Step != tbftpb.Step_PROPOSE ||
		!bytes.Equal(consensus.VerifingProposal.PbMsg.Key, hash) {
		consensus.logger.Warnf("[%s](%d/%d/%s) %x receive verify result (%d/%x) error",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			consensus.VerifingProposal.PbMsg.Key,
			height, hash,
		)
		consensus.VerifingProposal = nil
		return
	}

	consensus.Proposal = consensus.VerifingProposal
	consensus.Proposal.PbMsg.Content = verifyResult.VerifiedBatchMsg
	consensus.Proposal.PbMsg.TxsRwSet = verifyResult.TxsRwSet
	endTime := CurrentTimeMillisSeconds()

	consensus.logger.Infof("[%s](%d/%d/%s) processed verify result (%d/%x), time[verify:%dms]",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, height, hash,
		endTime-receiveResTime)

	// Prevote
	consensus.enterPrevote(consensus.Height, consensus.Round)
}

//
// handleBlockHeight
// @Description: Process block height messages and enter new heights
// @receiver consensus
// @param height
//
func (consensus *ConsensusTBFTImpl) handleBlockHeight(height uint64) {
	consensus.Lock()
	defer consensus.Unlock()

	consensus.logger.Infof("[%s](%d/%d/%s) receive block height %d",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, height)

	// Outdated block height event
	if consensus.Height > height {
		return
	}

	consensus.logger.Infof("[%s](%d/%d/%s) enterNewHeight because receiving block height %d",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, height)
	consensus.enterNewHeight(height + 1)
}

//
// procPropose
// @Description: Process proposal
// @receiver consensus
// @param proposal
//
func (consensus *ConsensusTBFTImpl) procPropose(proposal *Proposal) {
	receiveProposalTime := CurrentTimeMillisSeconds()
	if proposal == nil || proposal.Content == nil || proposal.Key == nil {
		consensus.logger.Warnf("[%s](%d/%d/%s) receive invalid proposal because nil proposal",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step)
		return
	}

	consensus.logger.Infof("[%s](%d/%d/%s) receive proposal from [%s], proposal(%d/%d/%x)",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		proposal.Proposer, proposal.Sequence, proposal.Round, proposal.Key,
	)

	// add future proposal in cache
	if consensus.Height < proposal.Sequence ||
		(consensus.Height == proposal.Sequence && consensus.Round < proposal.Round) {
		consensus.logger.Infof("[%s](%d/%d/%s) receive future proposal %s(%d/%d/%x)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			proposal.Proposer, proposal.Sequence, proposal.Round, proposal.Key)
		consensus.consensusFutureMsgCache.addFutureProposal(consensus.logger, consensus.validatorSet, proposal)
		return
	}

	height := proposal.Sequence
	if consensus.Height != height || consensus.Round != proposal.Round || consensus.Step != tbftpb.Step_PROPOSE {
		consensus.logger.Debugf("[%s](%d/%d/%s) receive invalid proposal: %s(%d/%d)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			proposal.Proposer, proposal.Sequence, proposal.Round)
		return
	}

	proposer, _ := consensus.validatorSet.GetProposer(consensus.lastHeightProposer, proposal.Sequence, proposal.Round)
	if proposer != proposal.Proposer {
		consensus.logger.Infof("[%s](%d/%d/%s) proposer: %s, receive proposal from incorrect proposal: %s",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, proposer, proposal.Proposer)
		return
	}

	// Proposals have been received, do not continue processing
	if consensus.Proposal != nil {
		if bytes.Equal(consensus.Proposal.PbMsg.Key, proposal.Key) {
			consensus.logger.Infof("[%s](%d/%d/%s) receive duplicate proposal from proposer: %s(%x)",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step, proposal.Proposer, proposal.Key)
		} else {
			consensus.logger.Infof("[%s](%d/%d/%s) receive unequal proposal from proposer: %s(%x)",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step, consensus.Proposal.PbMsg.Key,
				proposal.Proposer, proposal.Key)
		}
		return
	}

	// Proposal validating, do not proceed
	if consensus.VerifingProposal != nil {
		if bytes.Equal(consensus.VerifingProposal.PbMsg.Key, proposal.Key) {
			consensus.logger.Infof("[%s](%d/%d/%s) receive proposal which is verifying from proposer: %s(%x)",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				proposal.Proposer, proposal.Key)
		} else {
			consensus.logger.Infof("[%s](%d/%d/%s) receive unequal proposal with verifying proposal from proposer: %s(%x)",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				consensus.VerifingProposal.PbMsg.Key, proposal.Proposer, proposal.Key)
		}
		return
	}

	// this is a locked proposal, proc VerifyBlockWithRwSets by core
	// if verify succeed, we can enter Prevote
	if proposal.Qc != nil && proposal.TxsRwSet != nil {
		if err := consensus.procVerifyBlockWithRwSets(proposal); err != nil {
			consensus.logger.Warnf("[%s](%d/%d/%s) procVerifyBlockWithRwSetsl error: %v",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				err,
			)
			return
		}

		consensus.Proposal = consensus.NewTBFTProposal(proposal, false)
		// enter Prevote
		consensus.enterPrevote(consensus.Height, consensus.Round)
		return
	}

	procProposalTime := CurrentTimeMillisSeconds()
	consensus.VerifingProposal = consensus.NewTBFTProposal(proposal, false)
	// Tell the consensus module to perform block validation
	go consensus.procVerifyBatchMsg(proposal.Content)
	consensus.logger.Infof("[%s](%d/%d/%s) processed proposal (%d/%d/%x), time[verify:%dms]",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		proposal.Sequence, proposal.Round, proposal.Key,
		procProposalTime-receiveProposalTime)
}

// procVerifyBlockWithRwSets
// @Description: Process verify votes
// @receiver consensus
// @param proposal
//
func (consensus *ConsensusTBFTImpl) procVerifyBlockWithRwSets(proposal *Proposal) error {
	// verify qc
	vs, err := VerifyQcFromVotes(consensus.logger, consensus.verifier, proposal.Qc,
		consensus.validatorSet, tbftpb.VoteType_VOTE_PREVOTE)
	if err != nil {
		consensus.logger.Infof("[%s](%d/%d/%s) verify votes failed. from [%s](%d/%d/%x)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			proposal.Proposer, proposal.Sequence, proposal.Round, proposal.Key)
		return err
	}

	qc := vs.ToProto()
	err = consensus.verifier.VerifyBlockWithRwSets(proposal.Content, proposal.TxsRwSet, qc)
	return err
}

func (consensus *ConsensusTBFTImpl) procVerifyBatchMsg(batchMsg BatchMsg) {
	result, err := consensus.verifier.VerifyBatchMsg(batchMsg)
	if err != nil {
		consensus.logger.Infof("[%s](%d/%d/%s) procVerifyBatchMsg failed, %s(%v)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			batchMsg.Key(), err)
		return
	}

	consensus.verifyResultC <- result

}

// updateValidatorHeight update node height
// the height of a node is updated by vote so that the height of other nodes
// can be updated as soon as possible. only updated a higher node height
func (consensus *ConsensusTBFTImpl) updateValidatorHeight(vote *tbftpb.Vote) {
	consensus.validatorSet.Lock()
	defer consensus.validatorSet.Unlock()

	height := consensus.validatorSet.validatorsHeight[vote.Voter]
	if height < vote.Sequence {
		consensus.validatorSet.validatorsHeight[vote.Voter] = vote.Sequence
	}
	consensus.validatorSet.validatorsBeatTime[vote.Voter] = time.Now().UnixNano() / 1e6
}

//
// procPrevote
// @Description: Process vote
// @receiver consensus
// @param prevote
//
func (consensus *ConsensusTBFTImpl) procPrevote(prevote *tbftpb.Vote) {
	// update node height
	consensus.updateValidatorHeight(prevote)
	// add future prevote in cache
	if consensus.Height < prevote.Sequence {
		consensus.logger.Infof("[%s](%d/%d/%s) receive future prevote %s(%d/%d/%x)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			prevote.Voter, prevote.Sequence, prevote.Round, prevote.Hash)
		consensus.consensusFutureMsgCache.addFutureVote(consensus.logger, consensus.validatorSet, prevote)
		return
	}

	receivePrevoteTime := CurrentTimeMillisSeconds()
	// reject the vote that below consensus current height or locked round or current round or step
	if consensus.Height != prevote.Sequence ||
		consensus.LockedRound > prevote.Round ||
		consensus.Round > prevote.Round ||
		(consensus.Round == prevote.Round && consensus.Step > tbftpb.Step_PREVOTE) {
		errMsg := fmt.Sprintf("[%s](%d/%d/%s) receive invalid vote %s(%d/%d/%s)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			prevote.Voter, prevote.Sequence, prevote.Round, prevote.Type)
		consensus.logger.Debugf(errMsg)
		return
	}

	// judge whether we need this prevote vote before verifyVote
	// we allow locally generated prevoteVote to be added repeatedly
	// because the local node will not be a byzantine node
	if !consensus.heightRoundVoteSet.isRequired(prevote.Round, prevote) &&
		prevote.Voter != consensus.Id {
		consensus.logger.Debugf("receive prevote, but the vote is not required")
		return
	}
	verifyPrevoteTime := CurrentTimeMillisSeconds()

	consensus.logger.Infof("[%s](%d/%d/%s) receive prevote %s(%d/%d/%x), time[verify:%dms]",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		prevote.Voter, prevote.Sequence, prevote.Round, prevote.Hash,
		verifyPrevoteTime-receivePrevoteTime)

	err := consensus.addVote(prevote, false)
	if err != nil {
		consensus.logger.Errorf("[%s](%d/%d/%s) addVote %s(%d/%d/%s) failed, %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			prevote.Voter, prevote.Sequence, prevote.Round, prevote.Type, err,
		)
		return
	}
}

func (consensus *ConsensusTBFTImpl) procPrecommit(precommit *tbftpb.Vote) {
	// update node height
	consensus.updateValidatorHeight(precommit)
	// add future precommit in cache
	if consensus.Height < precommit.Sequence {
		consensus.logger.Infof("[%s](%d/%d/%s) receive future precommit %s(%d/%d/%x)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			precommit.Voter, precommit.Sequence, precommit.Round, precommit.Hash)
		consensus.consensusFutureMsgCache.addFutureVote(consensus.logger, consensus.validatorSet, precommit)
		return
	}

	receivePrecommitTime := CurrentTimeMillisSeconds()
	// reject the vote that below consensus current height or locked round or current round or step
	if consensus.Height != precommit.Sequence ||
		consensus.LockedRound > precommit.Round ||
		consensus.Round > precommit.Round ||
		(consensus.Round == precommit.Round && consensus.Step > tbftpb.Step_PRECOMMIT) {
		consensus.logger.Debugf("[%s](%d/%d/%s) receive invalid precommit %s(%d/%d)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			precommit.Voter, precommit.Sequence, precommit.Round,
		)
		return
	}

	// judge whether we need the precommit vote before verifyVote
	// we allow locally generated precommitVote to be added repeatedly
	// because the local node will not be a byzantine node
	if !consensus.heightRoundVoteSet.isRequired(precommit.Round, precommit) &&
		precommit.Voter != consensus.Id {
		consensus.logger.Debugf("receive precommit, but the vote is not required")
		return
	}
	verifyPrecommitTime := CurrentTimeMillisSeconds()

	consensus.logger.Infof("[%s](%d/%d/%s) receive precommit %s(%d/%d/%x), time[verify:%dms]",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		precommit.Voter, precommit.Sequence, precommit.Round, precommit.Hash,
		verifyPrecommitTime-receivePrecommitTime)

	err := consensus.addVote(precommit, false)
	if err != nil {
		consensus.logger.Errorf("[%s](%d/%d/%s) addVote %s(%d/%d/%s) failed, %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			precommit.Voter, precommit.Sequence, precommit.Round, precommit.Type, err,
		)
		return
	}
}

// fetch QC, quickly reach higher round
func (consensus *ConsensusTBFTImpl) procRoundQC(roundQC *tbftpb.RoundQC) {
	consensus.logger.Infof("[%s](%d/%d/%s) receive round qc from [%s](%d/%d/%x)",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		roundQC.Id, roundQC.Sequence, roundQC.Round, roundQC.Precommits.Maj23)

	// receive invalid round qc
	if roundQC.Sequence != consensus.Height || roundQC.Round <= consensus.Round {
		consensus.logger.Infof("[%s](%d/%d/%s) receive invalid round qc from [%s](%d/%d/%x)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			roundQC.Id, roundQC.Sequence, roundQC.Round, roundQC.Precommits.Maj23)
		return
	}
	// verify qc
	if roundQC.Precommits == nil && VerifyRoundQc(consensus.logger, consensus.verifier,
		consensus.validatorSet, roundQC) != nil {
		consensus.logger.Infof("[%s](%d/%d/%s) verify round qc failed. from [%s](%d/%d)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			roundQC.Id, roundQC.Sequence, roundQC.Round)
		return
	}

	if roundQC.Precommits.Type == tbftpb.VoteType_VOTE_PREVOTE {
		for _, vote := range roundQC.Precommits.Votes {
			_, err := consensus.heightRoundVoteSet.addVote(vote)
			if err != nil {
				consensus.logger.Infof("procRoundQC [%s](%d/%d/%s) addVote %v, err: %v",
					consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote, err)
				return
			}
		}

		// this is equivalent to completing the consensus step of prevote
		// enter prevote base on roundQC.Round
		consensus.Round = roundQC.Round
		consensus.Step = tbftpb.Step_PREVOTE
		consensus.enterPrecommit(consensus.Height, roundQC.Round)
		return
	}

	// this is equivalent to completing the consensus process of roundQC.Round
	// enter new round base on roundQC.Round+1
	if bytes.Equal(roundQC.Precommits.Maj23, nilHash) {
		consensus.enterNewRound(consensus.Height, roundQC.Round+1)
	}
}

func (consensus *ConsensusTBFTImpl) handleConsensusMsg(msg *ConsensusMsg) {
	consensus.Lock()
	defer consensus.Unlock()

	switch msg.Type {
	case tbftpb.TBFTMsgType_MSG_PROPOSE:
		consensus.procPropose(msg.Msg.(*Proposal))
	case tbftpb.TBFTMsgType_MSG_PREVOTE:
		consensus.procPrevote(msg.Msg.(*tbftpb.Vote))
	case tbftpb.TBFTMsgType_MSG_PRECOMMIT:
		consensus.procPrecommit(msg.Msg.(*tbftpb.Vote))
	case tbftpb.TBFTMsgType_MSG_SEND_ROUND_QC:
		consensus.procRoundQC(msg.Msg.(*tbftpb.RoundQC))
	}
}

// handleProposeOptimalTimeout handles timeout event
func (consensus *ConsensusTBFTImpl) handleProposeOptimalTimeout() {
	consensus.Lock()
	defer consensus.Unlock()

	consensus.logger.Infof("[%s](%d/%d/%s) handleProposeOptimalTimeout",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step)
	if consensus.Step != tbftpb.Step_PROPOSE {
		consensus.ProposeOptimalTimer.Stop()
		return
	}

	validator, _ := consensus.validatorSet.GetProposer(consensus.lastHeightProposer,
		consensus.Height, consensus.Round)
	timeNow := time.Now().UnixNano() / 1e6
	consensus.validatorSet.Lock()
	if timeNow > consensus.validatorSet.validatorsBeatTime[validator]+TimeDisconnet {
		consensus.enterPrevote(consensus.Height, consensus.Round)
	} else {
		consensus.ProposeOptimalTimer.Reset(TimeDisconnet * time.Millisecond)
	}
	consensus.validatorSet.Unlock()
}

// handleTimeout handles timeout event
func (consensus *ConsensusTBFTImpl) handleTimeout(ti tbftpb.TimeoutInfo) {
	consensus.Lock()
	defer consensus.Unlock()

	consensus.logger.Infof("[%s](%d/%d/%s) handleTimeout ti: %v",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, ti)

	switch ti.Step {
	case tbftpb.Step_PREVOTE:
		consensus.enterPrevote(ti.Sequence, ti.Round)
	case tbftpb.Step_PRECOMMIT:
		consensus.enterPrecommit(ti.Sequence, ti.Round)
	case tbftpb.Step_COMMIT:
		consensus.enterCommit(ti.Sequence, ti.Round)
	}
}

func (consensus *ConsensusTBFTImpl) commitBlock(batchMsg BatchMsg, voteSet *tbftpb.VoteSet) {
	beginTime := CurrentTimeMillisSeconds()
	err := consensus.committer.Commit(batchMsg, voteSet)
	if err != nil {
		consensus.logger.Infof("[%s](%d/%d/%s) consensus commit block failed",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step)
	}
	marshalTime := CurrentTimeMillisSeconds()

	consensus.logger.Infof("[%s](%d/%d/%s) consensus commit block , time[%dms]",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		marshalTime-beginTime)
}

// ProposeTimeout returns timeout to wait for proposing at `round`
func (consensus *ConsensusTBFTImpl) ProposeTimeout(round int32) time.Duration {
	return time.Duration(
		consensus.TimeoutPropose.Nanoseconds()+consensus.TimeoutProposeDelta.Nanoseconds()*int64(round),
	) * time.Nanosecond
}

// PrevoteTimeout returns timeout to wait for prevoting at `round`
func (consensus *ConsensusTBFTImpl) PrevoteTimeout(round int32) time.Duration {
	return time.Duration(
		TimeoutPrevote.Nanoseconds()+TimeoutPrevoteDelta.Nanoseconds()*int64(round),
	) * time.Nanosecond
}

// PrecommitTimeout returns timeout to wait for precommiting at `round`
func (consensus *ConsensusTBFTImpl) PrecommitTimeout(round int32) time.Duration {
	return time.Duration(
		TimeoutPrecommit.Nanoseconds()+TimeoutPrecommitDelta.Nanoseconds()*int64(round),
	) * time.Nanosecond
}

// CommitTimeout returns timeout to wait for precommiting at `round`
func (consensus *ConsensusTBFTImpl) CommitTimeout(round int32) time.Duration {
	return time.Duration(TimeoutCommit.Nanoseconds()*int64(round)) * time.Nanosecond
}

// AddTimeout adds timeout event to timeScheduler
func (consensus *ConsensusTBFTImpl) AddTimeout(duration time.Duration, height uint64, round int32,
	step tbftpb.Step) {
	consensus.timeScheduler.AddTimeoutInfo(tbftpb.TimeoutInfo{
		Duration: duration.Nanoseconds(),
		Sequence: height,
		Round:    round,
		Step:     step})
}

// addVote adds `vote` to heightVoteSet
func (consensus *ConsensusTBFTImpl) addVote(vote *tbftpb.Vote, replayMode bool) error {
	_, err := consensus.heightRoundVoteSet.addVote(vote)
	if err != nil {
		consensus.logger.Infof("[%s](%d/%d/%s) addVote %v, err: %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote, err)
		return err
	}

	switch vote.Type {
	case tbftpb.VoteType_VOTE_PREVOTE:
		consensus.addPrevoteVote(vote)
	case tbftpb.VoteType_VOTE_PRECOMMIT:
		consensus.addPrecommitVote(vote)
	}

	if consensus.Id == vote.Voter {
		// save prevote qc when send self precommit vote
		if vote.Type == tbftpb.VoteType_VOTE_PRECOMMIT && !replayMode {
			if consensus.Proposal != nil && bytes.Equal(consensus.Proposal.PbMsg.Key, vote.Hash) {
				// get prevote qc
				voteSet := consensus.heightRoundVoteSet.prevotes(consensus.Round)
				hash, ok := voteSet.twoThirdsMajority()
				walEntry := consensus.Proposal.PbMsg
				// get prevotes from voteSet by maj32
				if ok && !bytes.Equal(hash, nilHash) {
					// we save non-nil qc
					if walEntry.Qc != nil && len(walEntry.Qc) == 0 {
						hashStr := base64.StdEncoding.EncodeToString(hash)
						for _, v := range voteSet.VotesByBlock[hashStr].Votes {
							walEntry.Qc = append(walEntry.Qc, v)
						}
					}
					entry := &WalEntry{Proposal: walEntry}
					consensus.saveWalEntry(entry)
				}
			}
		}
		// Broadcast your vote to others
		payload := consensus.marshalConsensusVote(vote)
		_ = consensus.netHandler.BroadCastNetMsg(payload, "")
	}
	return nil
}

// marshalConsensusVote
// marshal prevote or precommit
func (consensus *ConsensusTBFTImpl) marshalConsensusVote(vote *tbftpb.Vote) []byte {
	if vote == nil {
		return nil
	}

	var msg *tbftpb.TBFTMsg
	switch vote.Type {
	case tbftpb.VoteType_VOTE_PREVOTE:
		msg = createPrevoteTBFTMsg(vote)
	case tbftpb.VoteType_VOTE_PRECOMMIT:
		msg = createPrecommitTBFTMsg(vote)
	}

	payload := mustMarshal(msg)
	consensus.logger.Infof("%s send [%s] vote %s", consensus.Id, vote.Type, vote.String())
	return payload
}

//
// addPrevoteVote
// @Description: add Prevote
// @receiver consensus
// @param vote
//
func (consensus *ConsensusTBFTImpl) addPrevoteVote(vote *tbftpb.Vote) {
	if consensus.Step != tbftpb.Step_PREVOTE {
		consensus.logger.Infof("[%s](%d/%d/%s) addVote prevote %s(%d/%d/%x) at inappropriate step",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			vote.Voter, vote.Sequence, vote.Round, vote.Hash)
		return
	}
	voteSet := consensus.heightRoundVoteSet.prevotes(vote.Round)
	hash, ok := voteSet.twoThirdsMajority()
	if !ok || voteSet.Round != consensus.Round {
		consensus.logger.Debugf("[%s](%d/%d/%s) addVote %v without majority",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote)

		if consensus.Round == vote.Round && voteSet.hasTwoThirdsAny() {
			consensus.logger.Infof("[%s](%d/%d/%s) addVote %v with hasTwoThirdsAny",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote)
			consensus.enterPrecommit(consensus.Height, consensus.Round)
		} else if consensus.Round == vote.Round && voteSet.hasTwoThirdsNoMajority() {
			// add the prevote timeout event
			consensus.logger.Infof("[%s](%d/%d/%s) addVote %v with hasTwoThirdsAny, PrevoteTimeout is igniting",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote)
			consensus.AddTimeout(consensus.PrevoteTimeout(consensus.Round), consensus.Height,
				consensus.Round, tbftpb.Step_PRECOMMIT)
		}
		return
	}

	// there is the action for unlocked
	// if we're locked but this is a recent majority32, so unlock.
	// if it matches our ProposalBlock, update the ValidBlock, else we need new round and get a new proposal
	// logic of Unlock: if " consensus.LockedRound < vote.Round <= consensus.Round "
	if (consensus.LockedProposal != nil) &&
		(consensus.LockedRound < vote.Round) &&
		(vote.Round <= consensus.Round) &&
		!bytes.Equal(hash, consensus.LockedProposal.Key) {
		consensus.logger.Debugf("unlocking because of a higher round maj32 ,locked_round: %d, height_round: %d",
			consensus.LockedRound, vote.Round)
		// there is the unlocked action ,it means that we are ready to receive new proposal and vote for it
		consensus.LockedRound = -1
		consensus.LockedProposal = nil
	}

	consensus.logger.Infof("[%s](%d/%d/%s) prevoteQC (%d/%d/%x)",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote.Sequence, vote.Round, hash)

	// Upon >2/3 prevotes, Step into StepPrecommit
	if consensus.Proposal != nil {
		// recent non-nil block of maj32 .
		if !isNilHash(hash) && (consensus.ValidRound < vote.Round) && (vote.Round == consensus.Round) {
			if bytes.Equal(hash, consensus.Proposal.PbMsg.Key) {
				consensus.logger.Debugf("updating valid block because of maj32, valid_round: %d, majority_round: %d",
					consensus.ValidRound, vote.Round)
				consensus.ValidRound = vote.Round
				consensus.ValidProposal = consensus.Proposal.PbMsg
				// ValidProposal need qc
				// The backup node receives a proposal containing qc, execute "VerifyBlockWithRwSets"
				hashStr := base64.StdEncoding.EncodeToString(hash)
				for _, v := range voteSet.VotesByBlock[hashStr].Votes {
					consensus.ValidProposal.Qc = append(consensus.ValidProposal.Qc, v)
				}
			} else {
				consensus.logger.Infof("proposal [%x] is not match majority[%x], need new round, set ProposalBlock=nil",
					consensus.Proposal.PbMsg.Key, hash)
				// we need a new round
				consensus.Proposal = nil
			}
		}
		consensus.enterPrecommit(consensus.Height, consensus.Round)
	} else {
		if isNilHash(hash) {
			consensus.enterPrecommit(consensus.Height, consensus.Round)
		} else {
			consensus.logger.Infof("[%s](%d/%d/%s) add vote failed, receive valid block: %x, but proposal is nil",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step, hash)
			consensus.enterPrecommit(consensus.Height, consensus.Round)
		}
	}
}

func (consensus *ConsensusTBFTImpl) delInvalidTxs(vs *VoteSet, hash []byte) {
	// del invalid txs
	if isNilHash(hash) {
		payload := vs.delInvalidTx(consensus.Height)
		if payload != nil {
			consensus.logger.Infof("[%s](%d/%d/%s) delete invalidTxs",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step)
			consensus.verifyFailTxs = payload
		}
	}
}

//
// addPrecommitVote
// @Description: add Precommit Vote
// @receiver consensus
// @param vote
//
func (consensus *ConsensusTBFTImpl) addPrecommitVote(vote *tbftpb.Vote) {
	if consensus.Step != tbftpb.Step_PRECOMMIT {
		consensus.logger.Infof("[%s](%d/%d/%s) addVote precommit %s(%d/%d/%x) at inappropriate step",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			vote.Voter, vote.Sequence, vote.Round, vote.Hash)
		return
	}

	voteSet := consensus.heightRoundVoteSet.precommits(vote.Round)
	hash, ok := voteSet.twoThirdsMajority()
	if !ok || voteSet.Round != consensus.Round {
		consensus.logger.Debugf("[%s](%d/%d/%s) addVote %v without majority",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote)

		if consensus.Round == vote.Round && voteSet.hasTwoThirdsAny() {
			consensus.logger.Infof("[%s](%d/%d/%s) addVote %v with hasTwoThirdsAny",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote)
			consensus.enterCommit(consensus.Height, consensus.Round)
		} else if consensus.Round == vote.Round && voteSet.hasTwoThirdsNoMajority() {
			// add the precommit timeout event
			consensus.logger.Infof("[%s](%d/%d/%s) addVote %v with hasTwoThirdsNoMajority, PrecommitTimeout is igniting",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote)
			consensus.AddTimeout(consensus.PrecommitTimeout(consensus.Round), consensus.Height,
				consensus.Round, tbftpb.Step_COMMIT)
		}
		return
	}

	consensus.logger.Infof("[%s](%d/%d/%s) precommitQC (%d/%d/%x)",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, vote.Sequence, vote.Round, hash)

	// Upon >2/3 precommits, Step into StepCommit
	if consensus.Proposal != nil {
		if isNilHash(hash) || bytes.Equal(hash, consensus.Proposal.PbMsg.Key) {
			consensus.enterCommit(consensus.Height, consensus.Round)
		} else {
			consensus.logger.Errorf("[%s](%d/%d/%s) block matched failed, receive valid block: %x,"+
				" but unmatched with proposal: %x",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				hash, consensus.Proposal.PbMsg.Key)
		}
	} else {
		if !isNilHash(hash) {
			consensus.logger.Warnf("[%s](%d/%d/%s) receive valid block: %x, but proposal is nil",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step, hash)
		}
		consensus.enterCommit(consensus.Height, consensus.Round)
	}
}

// enterNewHeight enter `height`
func (consensus *ConsensusTBFTImpl) enterNewHeight(height uint64) {
	consensus.logger.Infof("[%s]attempt enter new height to (%d)", consensus.Id, height)
	if consensus.Height >= height {
		consensus.logger.Errorf("[%s](%v/%v/%v) invalid enter invalid new height to (%v)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, height)
		return
	}

	//Update the validator in the consistency engine
	addedValidators, removedValidators, err := consensus.updateChainConfig()
	if err != nil {
		consensus.logger.Errorf("[%s](%v/%v/%v) update chain config failed: %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, err)
	}
	for i := 0; i < len(addedValidators); i++ {
		remoteState := &RemoteState{Id: addedValidators[i], Height: 0}
		status := make(map[int8]Status)
		status[remoteState.Type()] = remoteState
		remoteNodeStatus := &Node{id: addedValidators[i], status: status}
		err := consensus.consistentEngine.PutRemoter(remoteNodeStatus.ID(), remoteNodeStatus)
		if err != nil {
			consensus.logger.Errorf(err.Error())
			continue
		}
	}

	for i := 0; i < len(removedValidators); i++ {
		err := consensus.consistentEngine.RemoveRemoter(removedValidators[i])
		if err != nil {
			consensus.logger.Errorf(err.Error())
			continue
		}
	}

	consensus.consensusFutureMsgCache.updateConsensusHeight(consensus.Height)
	consensus.consensusStateCache.addConsensusState(consensus.ConsensusState)
	consensus.ConsensusState = NewConsensusState(consensus.logger, consensus.Id)
	consensus.Height = height
	consensus.validatorSet.Lock()
	consensus.validatorSet.validatorsHeight[consensus.Id] = height
	consensus.validatorSet.Unlock()
	consensus.Round = 0
	consensus.LockedRound = -1
	consensus.LockedProposal = nil
	consensus.ValidRound = -1
	consensus.ValidProposal = nil
	consensus.Step = tbftpb.Step_NEW_HEIGHT
	consensus.heightRoundVoteSet = newHeightRoundVoteSet(
		consensus.logger, consensus.Height, consensus.Round, consensus.validatorSet)
	consensus.metrics = newHeightMetrics(consensus.Height)
	consensus.metrics.SetEnterNewHeightTime()
	consensus.enterNewRound(height, 0)
}

// enterNewRound enter `round` at `height`
func (consensus *ConsensusTBFTImpl) enterNewRound(height uint64, round int32) {
	consensus.logger.Infof("[%s] attempt enterNewRound to (%d/%d)", consensus.Id, height, round)
	// get the Proposer of the last block commit height
	consensus.lastHeightProposer = consensus.getLastBlockProposer()
	if consensus.Height > height ||
		consensus.Round > round ||
		(consensus.Round == round && consensus.Step != tbftpb.Step_NEW_HEIGHT) {
		consensus.logger.Infof("[%s](%v/%v/%v) enter new round invalid(%v/%v)",

			consensus.Id, consensus.Height, consensus.Round, consensus.Step, height, round)
		return
	}
	consensus.Height = height
	consensus.Round = round
	consensus.Step = tbftpb.Step_NEW_ROUND
	consensus.Proposal = nil
	consensus.VerifingProposal = nil
	consensus.invalidTxs = nil
	consensus.metrics.SetEnterNewRoundTime(consensus.Round)
	consensus.enterPropose(height, round)
}

//
// enterPropose
// @Description: enter Propose
// @receiver consensus
// @param height
// @param round
//
func (consensus *ConsensusTBFTImpl) enterPropose(height uint64, round int32) {
	consensus.logger.Infof("[%s] attempt enterPropose to (%d/%d)", consensus.Id, height, round)
	consensus.ProposeOptimalTimer.Stop()
	if consensus.Height != height ||
		consensus.Round > round ||
		(consensus.Round == round && consensus.Step != tbftpb.Step_NEW_ROUND) {
		consensus.logger.Infof("[%s](%v/%v/%v) enter invalid propose(%v/%v)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, height, round)
		return
	}

	// Step into Propose
	consensus.Step = tbftpb.Step_PROPOSE
	consensus.metrics.SetEnterProposalTime(consensus.Round)

	// Calculate timeout
	validator, _ := consensus.validatorSet.GetProposer(consensus.lastHeightProposer, height, round)
	consensus.validatorSet.Lock()
	validatorHeight := consensus.validatorSet.validatorsHeight[validator]
	consensus.validatorSet.Unlock()

	timeout := consensus.getProposeTimeout(validator, validatorHeight, height, round)
	consensus.AddTimeout(timeout, height, round, tbftpb.Step_PREVOTE)

	if consensus.isProposer(height, round) {
		// create the proposal based on validProposal
		if consensus.ValidProposal != nil {
			proposalBlock := &ProposalBatchMsg{
				BatchMsg: consensus.ValidProposal.Content,
				TxsRwSet: consensus.ValidProposal.TxsRwSet,
			}
			consensus.proposedBlockC <- &proposedProposal{
				proposedBlock: proposalBlock,
				qc:            consensus.ValidProposal.Qc,
			}
		} else {
			// checks whether the local node need generate the proposal
			if consensus.validatorSet.checkProposed(consensus.Height) {
				consensus.sendProposeState(true)
			}
		}
	}

	// get proposal and vote from cache
	// notice : only the backup node possible get proposal from cache
	consensus.getFutureMsgFromCache(consensus.Height, consensus.Round)
}

// getProposeTimeout return timeout to wait for proposing at `round`
// we implement the optimization of timeout for proposed step. select two different processes
// through the config:
// 1. the lower the height of the proposer, the shorter ProposeTimeout for other nodes to wait for it
// 2. if the state of the proposer is len(validators) heights behind, the ProposeTimeout of the other nodes
// is TimeoutProposeOptimal
func (consensus *ConsensusTBFTImpl) getProposeTimeout(validator string, validatorHeight,
	height uint64, round int32) time.Duration {
	timeout := consensus.ProposeTimeout(round)
	// two different ways
	if consensus.ProposeOptimal {
		// local node or height 1, not optimized timeout
		if validator == consensus.Id || height <= 1 {
			return timeout
		}
		// if the state of the proposer is len(validators) heights behind or proposer disconnected
		// the ProposeTimeout of the other nodes is TimeoutProposeOptimal
		timeNow := time.Now().UnixNano() / 1e6
		if validatorHeight+uint64(len(consensus.validatorSet.Validators)) < height ||
			timeNow > consensus.validatorSet.validatorsBeatTime[validator]+TimeDisconnet {
			timeout = consensus.TimeoutProposeOptimal
		} else {
			// start propose optimal timer
			// we periodically check the connection status of proposer
			// the timer will be stopped at enterPrevote or entrPrepose
			consensus.ProposeOptimalTimer.Reset(TimeDisconnet * time.Millisecond)
		}
	} else {
		t := height - validatorHeight
		if height <= validatorHeight {
			t = 1
		}
		timeout = time.Duration(uint64(timeout) / t)
		if timeout < (2 * time.Second) {
			timeout = 2 * time.Second
		}
	}
	if timeout != consensus.ProposeTimeout(round) {
		consensus.logger.Infof("set timeout %d(height:%d,validatorHeight:%d,round:%d)",
			timeout, height, validatorHeight, round)
	}
	return timeout
}

func (consensus *ConsensusTBFTImpl) getFutureMsgFromCache(height uint64, round int32) {
	// only the backup node possible get proposal from cache
	// proposer is waiting proposal from core
	proposal := consensus.consensusFutureMsgCache.getConsensusFutureProposal(height, round)
	if proposal != nil {
		consensus.externalMsgC <- &ConsensusMsg{
			Type: tbftpb.TBFTMsgType_MSG_PROPOSE,
			Msg:  proposal,
		}
		consensus.logger.Infof("[%s](%d/%d/%s) cache future proposal %s(%d/%d/%x)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step,
			proposal.Proposer, proposal.Sequence, proposal.Round, proposal.Key)
	}

	// get vote from cache
	// put into the externalMsgC
	roundVs := consensus.consensusFutureMsgCache.getConsensusFutureVote(height, round)
	if roundVs != nil {
		for _, prevote := range roundVs.Prevotes.Votes {
			consensus.externalMsgC <- &ConsensusMsg{
				Type: tbftpb.TBFTMsgType_MSG_PREVOTE,
				Msg:  prevote,
			}
			consensus.logger.Infof("[%s](%d/%d/%s) get future prevote from cache. %s(%d/%d/%x)",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				prevote.Voter, prevote.Sequence, prevote.Round, prevote.Hash)
		}

		for _, precommit := range roundVs.Precommits.Votes {
			consensus.externalMsgC <- &ConsensusMsg{
				Type: tbftpb.TBFTMsgType_MSG_PRECOMMIT,
				Msg:  precommit,
			}
			consensus.logger.Infof("[%s](%d/%d/%s) get future precommit from cache. %s(%d/%d/%x)",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				precommit.Voter, precommit.Sequence, precommit.Round, precommit.Hash)
		}
	}
}

// enterPrevote enter `prevote` phase
func (consensus *ConsensusTBFTImpl) enterPrevote(height uint64, round int32) {
	consensus.ProposeOptimalTimer.Stop()
	if consensus.Height != height ||
		consensus.Round > round ||
		(consensus.Round == round && consensus.Step != tbftpb.Step_PROPOSE) {
		consensus.logger.Infof("[%s](%v/%v/%v) enter invalid prevote(%v/%v)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, height, round)
		return
	}

	enterPrevoteTime := CurrentTimeMillisSeconds()
	consensus.logger.Infof("[%s](%v/%v/%v) enter prevote (%v/%v)",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, height, round)

	// Enter StepPrevote
	consensus.Step = tbftpb.Step_PREVOTE
	consensus.metrics.SetEnterPrevoteTime(consensus.Round)

	// Disable propose
	consensus.sendProposeState(false)

	var hash = nilHash
	if consensus.LockedProposal != nil {
		// If a block is locked, prevote that.
		consensus.logger.Infof("prevote step; already locked on a block; prevoting locked block")
		hash = consensus.LockedProposal.Key
	} else if consensus.Proposal != nil {
		hash = consensus.Proposal.PbMsg.Key
	}

	// Broadcast prevote
	// prevote := createPrevoteProto(consensus.Id, consensus.Height, consensus.Round, hash)
	prevote := NewVote(tbftpb.VoteType_VOTE_PREVOTE, consensus.Id, consensus.Height, consensus.Round, hash)

	// if hash is nil
	if bytes.Equal(hash, nilHash) {
		prevote.InvalidTxs = consensus.invalidTxs
	}

	sign, err := consensus.verifier.SignVote(prevote)
	if err != nil {
		consensus.logger.Errorf("enter Prevote sign Vote error: %s", err)
		return
	}
	if prevote.Signature == nil {
		prevote.Signature = sign
	}
	signPrevoteTime := CurrentTimeMillisSeconds()

	prevoteMsg := createPrevoteConsensusMsg(prevote)
	consensus.internalMsgC <- prevoteMsg

	consensus.logger.Infof("[%s](%v/%v/%v) generated prevote (%d/%d/%x), time[sign:%dms]",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		prevote.Sequence, prevote.Round, prevote.Hash,
		signPrevoteTime-enterPrevoteTime)
}

// enterPrecommit enter `precommit` phase
func (consensus *ConsensusTBFTImpl) enterPrecommit(height uint64, round int32) {
	if consensus.Height != height ||
		consensus.Round > round ||
		(consensus.Round == round && consensus.Step != tbftpb.Step_PREVOTE) {
		consensus.logger.Infof("[%s](%v/%v/%v) enter precommit invalid(%v/%v)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, height, round)
		return
	}

	enterPrecommitTime := CurrentTimeMillisSeconds()
	consensus.logger.Infof("[%s](%v/%v/%v) enter precommit (%v/%v)",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, height, round)

	// Enter StepPrecommit
	consensus.Step = tbftpb.Step_PRECOMMIT
	consensus.metrics.SetEnterPrecommitTime(consensus.Round)

	voteSet := consensus.heightRoundVoteSet.prevotes(consensus.Round)
	hash, ok := voteSet.twoThirdsMajority()
	// del invalid txs
	consensus.delInvalidTxs(voteSet, hash)
	if !ok {
		if voteSet.hasTwoThirdsAny() || voteSet.hasTwoThirdsNoMajority() {
			hash = nilHash
			consensus.logger.Infof("[%s](%v/%v/%v) enter precommit to nil because hasTwoThirdsAny "+
				"or hasTwoThirdsNoMajority", consensus.Id, consensus.Height, consensus.Round, consensus.Step)
		} else {
			consensus.logger.Errorf("panic: this should not happen")
			panic("this should not happen")
		}
	} else {
		// There was a maj32 in the prevote set
		switch {
		case isNilHash(hash):
			// +2/3 prevoted nil. Unlock and precommit nil.
			if consensus.LockedProposal == nil {
				consensus.logger.Debugf("precommit step; +2/3 prevoted for nil")
			} else {
				consensus.logger.Debugf("precommit step; +2/3 prevoted for nil; unlocking")
				consensus.LockedRound = -1
				consensus.LockedProposal = nil
			}
		case consensus.LockedProposal != nil && bytes.Equal(hash, consensus.LockedProposal.Key):
			// If we're already locked on that block, precommit it, and update the LockedRound
			consensus.logger.Debugf("precommit step; +2/3 prevoted locked block; relocking")
			consensus.LockedRound = round
		case consensus.Proposal != nil && bytes.Equal(hash, consensus.Proposal.PbMsg.Key):
			// If +2/3 prevoted for proposal block, locked and precommit it
			consensus.logger.Debugf("precommit step; +2/3 prevoted proposal block; locking ,hash = %x", hash)
			consensus.LockedRound = round
			consensus.LockedProposal = consensus.Proposal.PbMsg
		default:
			// There was a maj32 in this round for a block we don't have
			consensus.logger.Debugf("precommit step; +2/3 prevotes for a block we do not have; voting nil")
			consensus.LockedRound = -1
			consensus.LockedProposal = nil
			hash = nilHash
		}
	}

	// Broadcast precommit
	precommit := NewVote(tbftpb.VoteType_VOTE_PRECOMMIT, consensus.Id, consensus.Height, consensus.Round, hash)
	sign, err := consensus.verifier.SignVote(precommit)
	if err != nil {
		consensus.logger.Errorf("enter Precommit sign Vote error: %s", err)
	}
	if precommit.Signature == nil {
		precommit.Signature = sign
	}
	signPrecommitTime := CurrentTimeMillisSeconds()

	precommitMsg := createPrecommitConsensusMsg(precommit)
	consensus.internalMsgC <- precommitMsg

	consensus.logger.Infof("[%s](%v/%v/%v) generated precommit (%d/%d/%x), time[sign:%dms]",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		precommit.Sequence, precommit.Round, precommit.Hash,
		signPrecommitTime-enterPrecommitTime)
}

// enterCommit enter `Commit` phase
func (consensus *ConsensusTBFTImpl) enterCommit(height uint64, round int32) {
	if consensus.Height != height ||
		consensus.Round > round ||
		(consensus.Round == round && consensus.Step != tbftpb.Step_PRECOMMIT) {
		consensus.logger.Infof("[%s](%d/%d/%s) enterCommit invalid(%v/%v)",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, height, round)
		return
	}

	consensus.logger.Infof("[%s](%d/%d/%s) enter commit (%v/%v)",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step, height, round)

	// Enter StepCommit
	consensus.Step = tbftpb.Step_COMMIT
	consensus.metrics.SetEnterCommitTime(consensus.Round)
	consensus.logger.Infof("[%s] consensus cost: %s", consensus.Id, consensus.metrics.roundString(consensus.Round))

	voteSet := consensus.heightRoundVoteSet.precommits(consensus.Round)
	hash, ok := voteSet.twoThirdsMajority()
	if !isNilHash(hash) && !ok {
		// This should not happen
		consensus.logger.Errorf("[%s]-%x, enter commit failed, without majority", consensus.Id, hash)
		panic(fmt.Errorf("[%s]-%x, enter commit failed, without majority", consensus.Id, hash))
	}

	if consensus.LockedProposal != nil && bytes.Equal(hash, consensus.LockedProposal.Key) {
		if consensus.Proposal == nil ||
			(consensus.Proposal != nil && !bytes.Equal(consensus.Proposal.PbMsg.Key,
				consensus.LockedProposal.Key)) {
			consensus.logger.Debugf("commit is for a locked block; set ProposalBlock=LockedBlock, hash = %x", hash)
			if consensus.isProposer(consensus.Height, consensus.Round) {
				consensus.Proposal = consensus.NewTBFTProposal(consensus.LockedProposal, true)
			} else {
				consensus.Proposal = consensus.NewTBFTProposal(consensus.LockedProposal, false)
			}
		}

	}

	if consensus.Proposal != nil && !bytes.Equal(hash, consensus.Proposal.PbMsg.Key) {
		hash = nilHash
	}

	if isNilHash(hash) || consensus.Proposal == nil {
		proposal := &Proposal{}
		if consensus.Proposal != nil && consensus.Proposal.PbMsg != nil {
			proposal = consensus.Proposal.PbMsg
		}
		consensus.rollBackC <- &ConsensusRollBack{
			Sequence:      consensus.Height,
			Proposal:      proposal,
			VerifyFailTxs: consensus.verifyFailTxs,
		}
		// consensus.AddTimeout(consensus.CommitTimeout(round), consensus.Height, round+1, tbftpb.Step_NEW_ROUND)
		consensus.enterNewRound(consensus.Height, round+1)
	} else {
		// Proposal block hash must be match with precommited block hash
		if !bytes.Equal(hash, consensus.Proposal.PbMsg.Key) {
			// This should not happen
			consensus.logger.Errorf("[%s] block match failed, unmatch precommit hash: %x with proposal hash: %x",
				consensus.Id, hash, consensus.Proposal.PbMsg.Key)
			panic(fmt.Errorf("[%s] block match failed, unmatch precommit hash: %x with proposal hash: %x",
				consensus.Id, hash, consensus.Proposal.PbMsg.Key))
		}

		// Commit block to core engine
		consensus.commitBlock(consensus.Proposal.PbMsg.Content, voteSet.ToProto())
	}
}

func isNilHash(hash []byte) bool {
	return len(hash) == 0 || bytes.Equal(hash, nilHash)
}

// isProposer returns true if this node is proposer at `height` and `round`,
// and returns false otherwise
func (consensus *ConsensusTBFTImpl) isProposer(height uint64, round int32) bool {
	proposer, _ := consensus.validatorSet.GetProposer(consensus.lastHeightProposer, height, round)

	return proposer == consensus.Id
}

// getLastBlockProposer returns the last block proposer nodeid
func (consensus *ConsensusTBFTImpl) getLastBlockProposer() string {
	return consensus.paramsHandler.GetLastBlockProposer()
}

//
// ToProto
// @Description: Copy *ConsensusState to *tbftpb.ConsensusState
// @receiver consensus
// @return *tbftpb.ConsensusState
//
func (consensus *ConsensusTBFTImpl) ToProto() *tbftpb.ConsensusState {
	consensus.RLock()
	defer consensus.RUnlock()
	msg := proto.Clone(consensus.toProto())
	return msg.(*tbftpb.ConsensusState)
}

// getValidatorSet get validator set from tbft
func (consensus *ConsensusTBFTImpl) getValidatorSet() *validatorSet {
	consensus.Lock()
	defer consensus.Unlock()
	return consensus.validatorSet
}

// saveWalEntry saves entry to Wal
func (consensus *ConsensusTBFTImpl) saveWalEntry(entry *WalEntry) {
	beginTime := CurrentTimeMillisSeconds()
	if consensus.walHandler == nil {
		return
	}

	entry.WalEntryType = tbftpb.WalEntryType_PROPOSAL_VOTE_ENTRY
	entry.Sequence = consensus.Height

	err := consensus.walHandler.Write(entry)
	if err != nil {
		consensus.logger.Fatalf("[%s](%d/%d/%s) save wal type: %s write error: %v",
			consensus.Id, consensus.Height, consensus.Round, consensus.Step, entry.WalEntryType, err)
	}
	endTime := CurrentTimeMillisSeconds()

	consensus.logger.Infof("[%s](%d/%d/%s) consensus save wal, "+
		"time[total:%dms]",
		consensus.Id, consensus.Height, consensus.Round, consensus.Step,
		endTime-beginTime)
}

// replayWal replays the wal when the node starting
func (consensus *ConsensusTBFTImpl) replayWal() error {
	currentHeight := consensus.startHeight

	// if no write wal now, enter new height directly
	if consensus.walHandler == nil {
		consensus.logger.Infof("[%s] no write wal, enter new height(%d) directly", consensus.Id, currentHeight+1)
		consensus.enterNewHeight(currentHeight + 1)
		return nil
	} else {
		consensus.logger.Infof("consensus.walHandler %v", consensus.walHandler)
	}

	// if write wal, need to replay wal
	lastEntry, err := consensus.walHandler.ReadLast()
	if err != nil || lastEntry == nil {
		consensus.logger.Infof("[%s] no write wal, enter new height(%d) directly", consensus.Id, currentHeight+1)
		consensus.enterNewHeight(currentHeight + 1)
		return nil
	}

	height := lastEntry.Sequence
	consensus.logger.Infof("[%s] replayWal chainHeight: %d and walHeight: %d",
		consensus.Id, currentHeight, height)

	if currentHeight+1 < height {
		consensus.logger.Fatalf("[%s] replay currentHeight: %v < height-1: %v, this should not happen",
			consensus.Id, currentHeight, height-1)
	}

	if currentHeight >= height {
		// consensus is slower than ledger
		consensus.enterNewHeight(currentHeight + 1)
		return nil
	}

	// replay wal log, currentHeight=height-1
	consensus.enterNewHeight(height)

	switch lastEntry.WalEntryType {
	case tbftpb.WalEntryType_PROPOSAL_VOTE_ENTRY:
		err := consensus.enterPrecommitFromReplayWal(lastEntry)
		if err != nil {
			return err
		}

	default:
		consensus.logger.Warnf("[%s] wal replay found unrecognized type[%v], this should not happen",
			consensus.Id, lastEntry.WalEntryType)
	}
	return nil
}

func (consensus *ConsensusTBFTImpl) enterPrecommitFromReplayWal(WalEntry *WalEntry) error {
	if WalEntry == nil || WalEntry.Proposal == nil {
		consensus.logger.Warnf("enterPrecommitFromReplayWal failed, the data from wal is unrecognized")
		return fmt.Errorf("the data from wal is unrecognized, data is null")
	}

	proposal := WalEntry.Proposal
	qc := proposal.Qc
	consensus.logger.Infof("[%s](%v/%v/%x) enter precommit from replay wal",
		consensus.Id, consensus.Height, proposal.Round, proposal.Key)

	if bytes.Equal(proposal.Key, nilHash) || !bytes.Equal(proposal.Content.Key(), qc[0].Hash) {
		consensus.logger.Warnf("enterPrecommitFromReplayWal failed, The proposal and the vote do not match")
		return fmt.Errorf("the proposal and the vote do not match")
	}
	// Enter Step_PREVOTE
	consensus.Step = tbftpb.Step_PREVOTE
	consensus.Proposal = consensus.NewTBFTProposal(proposal, true)
	consensus.Round = proposal.Round
	consensus.LockedProposal = proposal
	consensus.LockedRound = proposal.Round
	consensus.ValidProposal = proposal
	consensus.ValidRound = proposal.Round

	// replay prevote vote
	for _, v := range qc {
		_, err := consensus.heightRoundVoteSet.addVote(v)
		if err != nil {
			consensus.logger.Errorf("[%s](%d/%d/%s) addVote %s(%d/%d/%s) failed, %v",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				v.Voter, v.Sequence, v.Round, v.Type, err,
			)
			return fmt.Errorf("addVote failed")
		}
	}

	consensus.enterPrecommit(consensus.Height, proposal.Round)
	consensus.logger.Infof("enterPrecommitFromReplayWal succeed")
	return nil
}

//
// GetValidators
// @Description: Get validators from consensus state
// @receiver consensus
// @return []string validators
// @return error always return nil
//
func (consensus *ConsensusTBFTImpl) GetValidators() ([]string, error) {
	return consensus.validatorSet.Validators, nil
}

//
// GetLastHeight
// @Description: Get current height from consensus state
// @receiver consensus
// @return uint64
//
func (consensus *ConsensusTBFTImpl) GetLastHeight() uint64 {
	return consensus.Height
}

//
// GetConsensusStateJSON
// @Description: Get consensus status in json format
// @receiver consensus
// @return []byte
// @return error always return nil
//
func (consensus *ConsensusTBFTImpl) GetConsensusStateJSON() ([]byte, error) {

	//isProposer:=consensus.isProposer(consensus.Height, consensus.Round)
	cs := consensus.ConsensusState.toProto()
	return mustMarshal(cs), nil
}

//
// createConsensusMsgFromTBFTMsgBz
// @Description: Convert tbftMsgBz to *ConsensusMsg
// @param tbftMsgBz
// @return *ConsensusMsg
//
func (consensus *ConsensusTBFTImpl) createConsensusMsgFromTBFTMsgBz(tbftMsgBz []byte) *ConsensusMsg {
	tbftMsg := new(tbftpb.TBFTMsg)
	mustUnmarshal(tbftMsgBz, tbftMsg)

	switch tbftMsg.Type {
	case tbftpb.TBFTMsgType_MSG_PROPOSE:
		proposal, _ := consensus.proposalCoder.UnmarshalProposal(tbftMsg.Msg)
		if err := consensus.verifier.VerifyProposal(proposal); err != nil {
			consensus.logger.Warnf("[%s](%d/%d/%s) verify proposal error: %v",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				err,
			)
			return nil
		}
		return &ConsensusMsg{
			Type: tbftMsg.Type,
			Msg:  proposal,
		}
	case tbftpb.TBFTMsgType_MSG_PREVOTE:
		prevote := new(tbftpb.Vote)
		mustUnmarshal(tbftMsg.Msg, prevote)
		err := consensus.verifier.VerifyVote(prevote)
		if err != nil {
			consensus.logger.Errorf("[%s](%d/%d/%s) receive prevote %s(%d/%d/%x), verifyVote failed: %v",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				prevote.Voter, prevote.Sequence, prevote.Round, prevote.Hash, err,
			)
			return nil
		}
		return &ConsensusMsg{
			Type: tbftMsg.Type,
			Msg:  prevote,
		}
	case tbftpb.TBFTMsgType_MSG_PRECOMMIT:
		precommit := new(tbftpb.Vote)
		mustUnmarshal(tbftMsg.Msg, precommit)
		err := consensus.verifier.VerifyVote(precommit)
		if err != nil {
			consensus.logger.Errorf("[%s](%d/%d/%s) receive precommit %s(%d/%d/%x), verifyVote failed, %v",
				consensus.Id, consensus.Height, consensus.Round, consensus.Step,
				precommit.Voter, precommit.Sequence, precommit.Round, precommit.Hash, err,
			)
			return nil
		}
		return &ConsensusMsg{
			Type: tbftMsg.Type,
			Msg:  precommit,
		}
	case tbftpb.TBFTMsgType_MSG_FETCH_ROUNDQC:
		fetchRoundQC := new(tbftpb.FetchRoundQC)
		mustUnmarshal(tbftMsg.Msg, fetchRoundQC)
		return &ConsensusMsg{
			Type: tbftMsg.Type,
			Msg:  fetchRoundQC,
		}
	case tbftpb.TBFTMsgType_MSG_SEND_ROUND_QC:
		roundQC := new(tbftpb.RoundQC)
		mustUnmarshal(tbftMsg.Msg, roundQC)
		return &ConsensusMsg{
			Type: tbftMsg.Type,
			Msg:  roundQC,
		}
	case tbftpb.TBFTMsgType_MSG_STATE:
		state := new(tbftpb.GossipState)
		mustUnmarshal(tbftMsg.Msg, state)
		return &ConsensusMsg{
			Type: tbftMsg.Type,
			Msg:  state,
		}

	default:
		return nil
	}
}
