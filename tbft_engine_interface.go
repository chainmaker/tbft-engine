package tbft_engine

import (
	tbftpb "chainmaker.org/chainmaker/tbft-engine/pb"
	"context"
)

// TBFTConsensuEngine tbft共识算法引擎接口
type TBFTConsensuEngine interface {
	// 开始
	Start(ctx context.Context) error
	// 结束
	Stop() error
	// 注册网络处理器
	RegisterNetHandler(NetHandler) error
	// 注册共识内容处理器
	RegisterInterpreter(BatchMsgInterpreter) error
	// 注册共识验证器
	RegisterVerifyHandler(Verifier) error
	// 注册wal处理器
	RegisterWalHandler(WalHandler) error
	// 注册提交处理器
	RegisterCommitter(Committer) error
	// 注册日志处理器
	RegisterLogger(Logger) error
	// 注册共识参数处理器
	RegisterParamsHandler(ParamsHandler) error
	// 注册共识Proposal处理器
	RegisterProposalCoder(Coder) error
	// 获取共识状态
	Status() *tbftpb.Status
	// 通知调用者发起提案
	ProposeState() <-chan bool
	// 通知调用者这一轮共识失败
	RollBack() <-chan *ConsensusRollBack
}
