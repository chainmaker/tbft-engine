package tbft_engine

import (
	tbftpb "chainmaker.org/chainmaker/tbft-engine/pb"
	"time"
)

// 网络处理器
// 实现共识消息的收发处理
type NetHandler interface {
	// 发送消息
	// to是发送给指导者，to为""则是广播给所有节点
	BroadCastNetMsg([]byte, string) error
	// TBFT引擎通过监听此chan，可以获取到接收到的网络消息
	Listen() <-chan interface{}
}

// 共识内容处理器
// 实现共识内容的定义
type BatchMsgInterpreter interface {
	// TBFT引擎通过监听此chan, 获取一个BatchMsg进行共识
	PrepareBatchMsg() <-chan *ProposalBatchMsg
}

// 共识Proposal处理器
// 实现共识内容的定义
type Coder interface {
	// 实现Proposal的marshal
	MarshalProposal(*Proposal) ([]byte, error)
	// 实现Proposal的unmarshal
	UnmarshalProposal([]byte) (*Proposal, error)
}

// 提交者(持久化)
type Committer interface {
	// 提交batchMsg
	Commit(BatchMsg, *tbftpb.VoteSet) error
	// 完成batchMsg提交，通知共识开始下一个共识
	CommitDone() <-chan uint64
}

// 日志操作处理器
type Logger interface {
	Debug(args ...interface{})
	Debugf(format string, args ...interface{})
	Debugw(msg string, keysAndValues ...interface{})
	Error(args ...interface{})
	Errorf(format string, args ...interface{})
	Errorw(msg string, keysAndValues ...interface{})
	Warn(args ...interface{})
	Warnf(format string, args ...interface{})
	Warnw(msg string, keysAndValues ...interface{})
	Info(args ...interface{})
	Infof(format string, args ...interface{})
	Infow(msg string, keysAndValues ...interface{})
	Fatal(args ...interface{})
	Fatalf(format string, args ...interface{})
	Fatalw(msg string, keysAndValues ...interface{})
}

// 共识参数处理器
// 每次共识开始时的获取新的共识参数
type ParamsHandler interface {
	// 新的一次共识开始时调用，获取共识需要的参数
	GetNewParams() (validators []string, timeoutPropose time.Duration,
		timeoutProposeDelta time.Duration, tbftBlocksPerProposer uint64,
		proposeOptimal bool, TimeoutProposeOptimal time.Duration, err error)
	GetLastBlockProposer() string
}

// 共识验证器
// 实现Proposal和Vote的签名和验签
type Verifier interface {
	// 对Proposal签名
	SignProposal(*Proposal) (interface{}, error)
	// 对BatchMsg签名
	SignBatchMsg(BatchMsg) (interface{}, error)
	// 对vote签名
	SignVote(*tbftpb.Vote) ([]byte, error)
	// 验证Proposal
	VerifyProposal(*Proposal) error
	// 验证BatchMsg
	VerifyBatchMsg(BatchMsg) (*VerifyResult, error)
	// 通过读写集验证BatchMsg
	VerifyBlockWithRwSets(BatchMsg, interface{}, *tbftpb.VoteSet) error
	// 验证vote
	VerifyVote(*tbftpb.Vote) error
}

// wal处理器
// 实现读写共识消息的wal操作
type WalHandler interface {
	Write(*WalEntry) error
	// 读取一个最新的wal内容
	// tbft引擎启动时恢复只需要一个最新的wal内容
	ReadLast() (*WalEntry, error)
}

// 共识内容
type BatchMsg interface {
	// BatchMsg的索引
	Sequence() uint64
	// 获取BatchMsg的Key
	Key() []byte
	// 获取BatchMsg签名
	GetSetSignature() interface{}
	// 设置BatchMsg签名
	SetSignature(interface{})
}
