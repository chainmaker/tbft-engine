/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package tbft_engine

type TBFTProposal struct {
	PbMsg *Proposal
	// byte format *tbftpb.Proposal
	Bytes []byte
}

// NewTBFTProposal create tbft proposal instance
func (consensus *ConsensusTBFTImpl) NewTBFTProposal(proposal *Proposal, marshal bool) *TBFTProposal {
	tbftProposal := &TBFTProposal{
		PbMsg: proposal,
	}
	if marshal {
		var err error
		// need marshal
		tbftProposal.Bytes, err = consensus.proposalCoder.MarshalProposal(proposal)
		if err != nil {
			panic("proposalHandler MustMarshalProposal failed")
		}
	}
	return tbftProposal
}
