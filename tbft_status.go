package tbft_engine

import (
	"sync"
	"time"

	tbftpb "chainmaker.org/chainmaker/tbft-engine/pb"
)

const (
	TypeRemoteTBFTState = 1
	TypeLocalTBFTState  = 2
)

// Node 节点信息（local/remoter）
type Node struct {
	sync.RWMutex
	id     string
	status map[int8]Status
}

func (l *Node) ID() string {
	return l.id
}

func (l *Node) Statuses() map[int8]Status {
	l.RLock()
	defer l.RUnlock()
	return l.status
}

// UpdateStatus 更新节点状态
func (l *Node) UpdateStatus(s Status) {
	l.Lock()
	defer l.Unlock()

	status := l.status[s.Type()]
	status.Update(s)
}

//
// RemoteState
// @Description: Validator status, validator and remote are the same
//
type RemoteState struct {
	sync.RWMutex
	//node id
	Id string
	//current height
	Height uint64
	// current round
	Round int32
	// current step
	Step tbftpb.Step

	// proposal
	Proposal         []byte
	VerifingProposal []byte
	LockedRound      int32
	// locked proposal
	LockedProposal *tbftpb.Proposal
	ValidRound     int32
	// valid proposal
	ValidProposal *tbftpb.Proposal
	RoundVoteSet  *roundVoteSet
}

func (r *RemoteState) Update(state Status) {
	r.Lock()
	defer r.Unlock()
	local, ok := state.(*RemoteState)
	if !ok || local.Id != r.Id {
		return
	}
	r.Height = local.Height
	r.Round = local.Round
	r.Step = local.Step
	r.Proposal = local.Proposal
	r.ValidProposal = local.ValidProposal
	r.RoundVoteSet = local.RoundVoteSet
}

func (r *RemoteState) Type() int8 {
	return TypeRemoteTBFTState
}

func (r *RemoteState) Data() interface{} {
	return r
}

type StatusDecoder struct {
	log      Logger
	tbftImpl *ConsensusTBFTImpl
}

// Decode 解析消息，返回节点状态
func (tD *StatusDecoder) Decode(d interface{}) interface{} {
	state, ok := d.(*tbftpb.GossipState)
	if !ok {
		tD.log.Debugf("not tbftpb.GossipState")
		return nil
	}

	rs := &RemoteState{
		Id:               state.Id,
		Height:           state.Height,
		Round:            state.Round,
		Step:             state.Step,
		Proposal:         state.Proposal,
		VerifingProposal: state.VerifingProposal,
		LockedRound:      0,
		LockedProposal:   nil,
		ValidRound:       0,
		ValidProposal:    nil,
		RoundVoteSet:     tD.newRoundVoteSet(state.RoundVoteSet),
	}

	// Update the block height of the validator node in the tbft module
	tD.tbftImpl.validatorSet.Lock()
	if tD.tbftImpl.validatorSet.validatorsHeight[state.Id] < state.Height {
		tD.tbftImpl.validatorSet.validatorsHeight[state.Id] =
			state.Height
	}
	tD.tbftImpl.validatorSet.validatorsBeatTime[state.Id] = time.Now().UnixNano() / 1e6
	tD.tbftImpl.validatorSet.Unlock()

	// fetch votes from this node state
	if tD.tbftImpl != nil && rs.Height == tD.tbftImpl.Height && rs.Round == tD.tbftImpl.Round &&
		rs.RoundVoteSet != nil {
		tD.log.Debugf("[%s] updateVoteWithProto: [%d/%d]", rs.Id, rs.Height, rs.Round)
		tD.updateVoteWithProto(rs.RoundVoteSet, rs.Round)
	}

	rss := make(map[int8]Status)
	rss[rs.Type()] = rs
	remoteInfo := &Node{id: state.Id, status: rss}

	return remoteInfo
}

// get the votes for tbft Engine based on the remote state
func (tD *StatusDecoder) updateVoteWithProto(voteSet *roundVoteSet, stateRound int32) {
	validators := tD.tbftImpl.getValidatorSet().Validators
	tD.tbftImpl.RLock()
	defer tD.tbftImpl.RUnlock()
	for _, voter := range validators {
		tD.log.Debugf("%s updateVoteWithProto : %v,%v", voter, voteSet.Prevotes, voteSet.Precommits)
		// prevote Vote
		vote := voteSet.Prevotes.Votes[voter]
		if vote != nil && tD.tbftImpl.Step < tbftpb.Step_PRECOMMIT &&
			tD.tbftImpl.Id != vote.Voter &&
			tD.tbftImpl.heightRoundVoteSet.isRequired(stateRound, vote) {
			tD.log.Debugf("updateVoteWithProto prevote : %s", voter)
			if err := tD.tbftImpl.verifier.VerifyVote(vote); err != nil {
				tD.log.Debugf("updateVoteWithProto prevote verify faield: %v", err)
				continue
			}
			tbftMsg := createPrevoteConsensusMsg(vote)
			tD.tbftImpl.internalMsgC <- tbftMsg
		}
		// precommit Vote
		vote = voteSet.Precommits.Votes[voter]
		if vote != nil && tD.tbftImpl.Step < tbftpb.Step_COMMIT &&
			tD.tbftImpl.Id != vote.Voter &&
			tD.tbftImpl.heightRoundVoteSet.isRequired(stateRound, vote) {
			tD.log.Debugf("updateVoteWithProto precommit : %s", voter)
			if err := tD.tbftImpl.verifier.VerifyVote(vote); err != nil {
				tD.log.Debugf("updateVoteWithProto precommit verify faield: %v", err)
				continue
			}
			tbftMsg := createPrecommitConsensusMsg(vote)
			tD.tbftImpl.internalMsgC <- tbftMsg
		}
	}
}

// 创建新轮次投票集合
func (tD *StatusDecoder) newRoundVoteSet(rvs *tbftpb.RoundVoteSet) *roundVoteSet {
	if rvs == nil {
		return nil
	}
	return &roundVoteSet{
		Height:     rvs.Sequence,
		Round:      rvs.Round,
		Prevotes:   tD.newVoteSet(rvs.Prevotes),
		Precommits: tD.newVoteSet(rvs.Precommits),
	}
}

// 创建投票集合
func (tD *StatusDecoder) newVoteSet(vs *tbftpb.VoteSet) *VoteSet {
	voteSet := &VoteSet{
		logger:       tD.log.(Logger),
		Type:         vs.Type,
		Height:       vs.Sequence,
		Round:        vs.Round,
		Votes:        make(map[string]*tbftpb.Vote),
		VotesByBlock: make(map[string]*BlockVotes),
		validators:   nil, //本模块不使用
	}
	for _, v := range vs.Votes {
		added, err := voteSet.AddVoteForConsistent(v)
		if !added || err != nil {
			tD.log.Errorf("err: %s", err)
		}
	}
	return voteSet
}
