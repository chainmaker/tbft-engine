package tbft_engine

import (
	"errors"
	"time"

	tbftpb "chainmaker.org/chainmaker/tbft-engine/pb"

	"github.com/gogo/protobuf/proto"
)

const (
	// TimerInterval 定时器间隔
	TimerInterval = 500 * time.Millisecond
	// MessageBufferSize 缓存消息大小
	MessageBufferSize = 10240

	// StatusBroadcasterTbft TBFT状态广播器ID
	StatusBroadcasterTbft = "TBFT"
	// InterceptorTbft 标识
	InterceptorTbft = 0
)

type StatusBroadcaster struct {
	log Logger

	// broadcaster 标识
	id string
	// 运行状态
	running bool
}

func (tsb *StatusBroadcaster) IsRunning() bool {
	return tsb.running
}

func (tsb *StatusBroadcaster) Start() error {
	tsb.running = true
	return nil
}

func (tsb *StatusBroadcaster) Stop() error {
	tsb.running = false
	return nil
}

func NewTBFTStatusBroadcaster(log Logger) *StatusBroadcaster {
	bsb := &StatusBroadcaster{
		log:     log,
		id:      StatusBroadcasterTbft,
		running: false,
	}
	return bsb
}

func (tsb *StatusBroadcaster) ID() string {
	return tsb.id
}

//TimePattern 状态广播触发模式
func (tsb *StatusBroadcaster) TimePattern() interface{} {
	return TimerInterval
}

//PreBroadcaster 消息广播前做前置处理，如状态校验/判断是否要发送消息等
func (tsb *StatusBroadcaster) PreBroadcaster() Broadcast {
	return func(localInfo *Node, remoteInfo *Node) (interface{}, error) {
		tsb.log.Debugf("local id:%s remote id:%s",
			localInfo.ID(), remoteInfo.ID())

		var netMSGs []*tbftpb.NetMsg
		if localInfo.ID() == "" || remoteInfo.ID() == "" {
			return nil, errors.New("error Node")
		}

		local, ok := localInfo.Statuses()[TypeLocalTBFTState].(*ConsensusTBFTImpl)
		if !ok {
			tsb.log.Warnf("not *ConsensusTBFTImpl type")
		}
		remoter, ok := remoteInfo.Statuses()[TypeRemoteTBFTState].(*RemoteState)
		if !ok {
			tsb.log.Warnf("not *RemoteState type")
		}

		//发送状态state
		sendState(local, remoteInfo, remoter, &netMSGs)

		if local == nil || remoter == nil || remoter.Height == 0 || local.Height < remoter.Height {
			return netMSGs, nil
		} else if local.Height == remoter.Height {
			local.RLock()
			defer local.RUnlock()
			//发送提案
			sendProposalOfRound(local, remoteInfo, remoter, &netMSGs)

			//发送prevote
			sendPrevoteOfRound(local, remoteInfo, remoter, &netMSGs)

			//发送Precommit
			sendPrecommitOfRound(local, remoteInfo, remoter, &netMSGs)

			//发送RoundQc
			sendRoundQC(local, remoteInfo, remoter, &netMSGs)

		} else {
			local.RLock()
			defer local.RUnlock()
			//远端节点落后很多，取本地缓存的历史高度状态，发送给远端节点
			state := local.consensusStateCache.getConsensusState(remoter.Height)
			if state == nil {
				tsb.log.Debugf("[%s] no caching consensusState, height:%d", local.Id, remoter.Height)
			} else {
				//发送Proposal
				sendProposalInState(state, remoter, &netMSGs)

				//发送Prevote
				sendPrevoteInState(local, state, remoter, &netMSGs)

				//发送Precommit
				sendPrecommitInState(local, state, remoter, &netMSGs)
			}
		}
		return netMSGs, nil
	}
}

type StatusInterceptor struct {
}

func (tsb *StatusInterceptor) Handle(status Status) error {
	if status.Type() != TypeLocalTBFTState &&
		status.Type() != TypeRemoteTBFTState {
		return errors.New("error status type")
	}

	return nil
}

//将本地节点（local）最新状态（state）发送给远端节点（remote）
func sendState(local *ConsensusTBFTImpl, remoteInfo *Node,
	remoter *RemoteState, netMSGs *[]*tbftpb.NetMsg) {
	local.RLock()
	defer local.RUnlock()

	var proposal []byte
	if local.Proposal != nil {
		proposal = local.Proposal.PbMsg.Key
	}

	var verifingProposal []byte
	if local.Proposal != nil {
		verifingProposal = local.Proposal.PbMsg.Key
	}

	gossipProto := &tbftpb.GossipState{
		Id:               local.Id,
		Height:           local.Height,
		Round:            local.Round,
		Step:             local.Step,
		Proposal:         proposal,
		VerifingProposal: verifingProposal,
	}
	if local.heightRoundVoteSet.getRoundVoteSet(local.Round) != nil {
		gossipProto.RoundVoteSet = local.heightRoundVoteSet.getRoundVoteSet(local.Round).ToProto()
	}
	msg := proto.Clone(gossipProto)

	tbftMsg := &tbftpb.TBFTMsg{
		Type: tbftpb.TBFTMsgType_MSG_STATE,
		Msg:  mustMarshal(msg),
	}

	netMsg := &tbftpb.NetMsg{
		Payload: mustMarshal(tbftMsg),
		To:      remoteInfo.ID(),
	}
	*netMSGs = append(*netMSGs, netMsg)
	local.logger.Infof("[%s](%d/%d/%s) send state to [%s](%d/%d/%s)", local.Id, local.Height, local.Round, local.Step,
		remoteInfo.ID(), remoter.Height, remoter.Round, remoter.Step)
}

//将本地节点（local）最新的提案（proposal）发送给远端节点（remote）
func sendProposalOfRound(local *ConsensusTBFTImpl, remoteInfo *Node,
	remoter *RemoteState, netMSGs *[]*tbftpb.NetMsg) {
	if local.Proposal != nil && local.Proposal.Bytes != nil && local.Proposal.PbMsg != nil {
		msg := createProposalTBFTMsg(local.Proposal)
		netMsg := &tbftpb.NetMsg{
			Payload: mustMarshal(msg),
			To:      remoteInfo.ID(),
		}
		*netMSGs = append(*netMSGs, netMsg)
		local.logger.Infof("[%s](%d/%d/%s) send proposal [%d/%d/%x] to [%s](%d/%d/%s)", local.Id, local.Height, local.Round,
			local.Step, local.Proposal.PbMsg.Sequence, local.Proposal.PbMsg.Round, local.Proposal.PbMsg.Content.Key(),
			remoteInfo.ID(), remoter.Height, remoter.Round, remoter.Step)
	}
}

//将本地节点（local）最新的预投票（prevote）发送给远端节点（remote）
func sendPrevoteOfRound(local *ConsensusTBFTImpl,
	remoteInfo *Node,
	remoter *RemoteState, netMSGs *[]*tbftpb.NetMsg) {
	remoter.RLock()
	defer remoter.RUnlock()
	prevoteVs := local.heightRoundVoteSet.prevotes(remoter.Round)
	if prevoteVs != nil {
		vote, ok := prevoteVs.Votes[local.Id]
		if ok && remoter.RoundVoteSet != nil && remoter.RoundVoteSet.Prevotes != nil {

			if _, pOk := remoter.RoundVoteSet.Prevotes.Votes[local.Id]; !pOk {
				msg := createPrevoteTBFTMsg(vote)
				netMsg := &tbftpb.NetMsg{
					Payload: mustMarshal(msg),
					To:      remoteInfo.ID(),
				}
				local.logger.Infof("[%s](%d/%d/%s) send prevote [%d/%d/%x] to [%s](%d/%d/%s)", local.Id, local.Height, local.Round,
					local.Step, vote.Sequence, vote.Round, vote.Hash, remoteInfo.ID(), remoter.Height, remoter.Round, remoter.Step)
				*netMSGs = append(*netMSGs, netMsg)
			}
		}
	}
}

//将本地节点（local）最新的预提交（precommit）发送给远端节点（remote）
func sendPrecommitOfRound(local *ConsensusTBFTImpl,
	remoteInfo *Node,
	remoter *RemoteState, netMSGs *[]*tbftpb.NetMsg) {
	remoter.RLock()
	defer remoter.RUnlock()
	precommitVs := local.heightRoundVoteSet.precommits(remoter.Round)
	if precommitVs != nil {
		vote, ok := precommitVs.Votes[local.Id]
		if ok && remoter.RoundVoteSet != nil && remoter.RoundVoteSet.Precommits != nil {

			if _, pOk := remoter.RoundVoteSet.Precommits.Votes[local.Id]; !pOk {
				msg := createPrecommitTBFTMsg(vote)
				netMsg := &tbftpb.NetMsg{
					Payload: mustMarshal(msg),
					To:      remoteInfo.ID(),
				}
				local.logger.Infof("[%s](%d/%d/%s) send precommit [%d/%d/%x] to [%s](%d/%d/%s)", local.Id, local.Height,
					local.Round, local.Step, vote.Sequence, vote.Round, vote.Hash, remoteInfo.ID(), remoter.Height,
					remoter.Round, remoter.Step)
				*netMSGs = append(*netMSGs, netMsg)
			}
		}
	}
}

//将本地节点（local）历史高度（Height）的状态（state）发送给远端节点（remote）
func sendProposalInState(state *ConsensusState, remoter *RemoteState, netMSGs *[]*tbftpb.NetMsg) {
	// only the proposer has the serialized proposal
	if state.Proposal != nil && state.Proposal.Bytes != nil && state.Proposal.PbMsg != nil {
		msg := createProposalTBFTMsg(state.Proposal)
		netMsg := &tbftpb.NetMsg{
			Payload: mustMarshal(msg),
			To:      remoter.Id,
		}
		state.logger.Infof("[%s](%d/%d/%s) send proposal [%d/%d/%x] to [%s](%d/%d/%s)", state.Id, state.Height, state.Round,
			state.Step, state.Proposal.PbMsg.Sequence, state.Proposal.PbMsg.Round, state.Proposal.PbMsg.Content.Key(),
			remoter.Id, remoter.Height, remoter.Round, remoter.Step)
		*netMSGs = append(*netMSGs, netMsg)
	}
}

//将本地节点（local）历史高度（Height）的预投票（prevote）发送给远端节点（remote）
func sendPrevoteInState(local *ConsensusTBFTImpl, state *ConsensusState,
	remoter *RemoteState, netMSGs *[]*tbftpb.NetMsg) {
	remoter.RLock()
	defer remoter.RUnlock()
	prevoteVs := state.heightRoundVoteSet.prevotes(remoter.Round)
	if prevoteVs != nil {
		vote, ok := prevoteVs.Votes[local.Id]
		roundVoteSet := remoter.RoundVoteSet
		if ok && roundVoteSet != nil && roundVoteSet.Prevotes != nil {
			if _, pOk := roundVoteSet.Prevotes.Votes[local.Id]; !pOk {
				msg := createPrevoteTBFTMsg(vote)
				netMsg := &tbftpb.NetMsg{
					Payload: mustMarshal(msg),
					To:      remoter.Id,
				}
				local.logger.Infof("[%s](%d/%d/%s) send prevote[%d/%d/%x] to [%s](%d/%d/%s)", local.Id, state.Height, state.Round,
					state.Step, vote.Sequence, vote.Round, vote.Hash, remoter.Id, remoter.Height, remoter.Round, remoter.Step)
				*netMSGs = append(*netMSGs, netMsg)
			}
		}
	}
}

//将本地节点（local）历史高度（Height）的预提案（precommit）发送给远端节点（remote）
func sendPrecommitInState(local *ConsensusTBFTImpl, state *ConsensusState,
	remoter *RemoteState, netMSGs *[]*tbftpb.NetMsg) {
	remoter.RLock()
	defer remoter.RUnlock()
	precommitVs := state.heightRoundVoteSet.precommits(remoter.Round)
	if precommitVs != nil {
		vote, ok := precommitVs.Votes[local.Id]
		roundVoteSet := remoter.RoundVoteSet
		if ok && roundVoteSet != nil && roundVoteSet.Precommits != nil {
			if _, pOk := roundVoteSet.Precommits.Votes[local.Id]; !pOk {
				msg := createPrecommitTBFTMsg(vote)
				netMsg := &tbftpb.NetMsg{
					Payload: mustMarshal(msg),
					To:      remoter.Id,
				}
				local.logger.Infof("[%s](%d/%d/%s) send precommit[%d/%d/%x] to [%s](%d/%d/%s)", local.Id, state.Height, state.Round,
					state.Step, vote.Sequence, vote.Round, vote.Hash, remoter.Id, remoter.Height, remoter.Round, remoter.Step)
				*netMSGs = append(*netMSGs, netMsg)
			}
		}
	}
}

// 发送QC，用于Round快速同步
// 一段时间没有tx，各节点Height不变，Round持续增加，
// 当Round落后当节点启动时，能快速同步到相同Round。
func sendRoundQC(local *ConsensusTBFTImpl,
	remoteInfo *Node,
	remoter *RemoteState, netMSGs *[]*tbftpb.NetMsg) {
	remoter.RLock()
	defer remoter.RUnlock()
	//高度相同，Round落后超过1时，使用快速同步
	if local.Height == remoter.Height && local.Round > remoter.Round {
		var voteSet *VoteSet
		for round := local.Round; round >= remoter.Round; round-- {
			roundVoteSet := local.heightRoundVoteSet.getRoundVoteSet(round)
			if roundVoteSet == nil {
				continue
			}

			// get precommit QC
			if roundVoteSet.Precommits != nil {
				_, ok := roundVoteSet.Precommits.twoThirdsMajority()
				// we need a QC
				if ok {
					voteSet = roundVoteSet.Precommits
					break
				}
			}

			// get prevote QC
			if roundVoteSet.Prevotes != nil {
				_, ok := roundVoteSet.Prevotes.twoThirdsMajority()
				// we need a QC
				if ok {
					voteSet = roundVoteSet.Prevotes
					break
				}
			}
		}

		if voteSet != nil {
			roundQC := &tbftpb.RoundQC{
				Id:         local.Id,
				Sequence:   voteSet.Height,
				Round:      voteSet.Round,
				Precommits: voteSet.ToProto(),
			}

			tbftMsg := &tbftpb.TBFTMsg{
				Type: tbftpb.TBFTMsgType_MSG_SEND_ROUND_QC,
				Msg:  mustMarshal(roundQC),
			}
			netMsg := &tbftpb.NetMsg{
				Payload: mustMarshal(tbftMsg),
				To:      remoter.Id,
			}

			local.logger.Infof("[%s](%d/%d/%s) send roundqc [%d/%d/%s] to [%s](%d/%d/%s)", local.Id, local.Height,
				local.Round, local.Step, voteSet.Height, voteSet.Round, voteSet.Type, remoteInfo.ID(), remoter.Height,
				remoter.Round, remoter.Step)
			*netMSGs = append(*netMSGs, netMsg)
		}
	}
}

type TbftConsistentMessage struct {
	log        Logger
	netHandler NetHandler
	messageC   chan *tbftpb.GossipState
}

func (m *TbftConsistentMessage) OnQuit() {
	close(m.messageC)
}

func NewTbftConsistentMessage(log Logger, netHandler NetHandler) *TbftConsistentMessage {
	tcm := &TbftConsistentMessage{
		messageC:   make(chan *tbftpb.GossipState, MessageBufferSize),
		netHandler: netHandler,
		log:        log,
	}
	return tcm
}

func (m *TbftConsistentMessage) Send(payload interface{}) {
	netMsg, ok := payload.(*tbftpb.NetMsg)
	if !ok {
		m.log.Debugf("send msg to %s failed", netMsg.To)
	}
	m.log.Debugf("send msg to %s", netMsg.To)
	_ = m.netHandler.BroadCastNetMsg(netMsg.Payload, netMsg.To)
}

func (m *TbftConsistentMessage) Receive() interface{} {
	m.log.Debugf("receive len(m.messageC)=%d", len(m.messageC))

	return <-m.messageC
}

func (m *TbftConsistentMessage) AddNetMsg(payload interface{}) {
	state, ok := payload.(*tbftpb.GossipState)
	if !ok {
		m.log.Debugf("AddNetMsg failed , is not tbftpb.GossipState")
	}
	m.messageC <- state
}
