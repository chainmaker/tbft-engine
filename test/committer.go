package test

import (
	tbftengine "chainmaker.org/chainmaker/tbft-engine"
	tbftpb "chainmaker.org/chainmaker/tbft-engine/pb"
	"errors"
)

var _ tbftengine.Committer = &Committer{}

// Committer 提交者(持久化)
type Committer struct {
	log          *Logger
	blockHeightC chan uint64
	BlockHeight  uint64
}

func (c *Committer) Name() string {
	return CommitterName
}

func (c *Committer) Close() {
}

func NewCommitter(log *Logger, size int) *Committer {
	c := &Committer{
		log:          log,
		blockHeightC: make(chan uint64, size),
	}

	return c
}

// Commit 提交batchMsg
func (c *Committer) Commit(batchMsg tbftengine.BatchMsg, voteSet *tbftpb.VoteSet) error {
	bm, ok := batchMsg.(*BatchMsg)
	if !ok {
		return errors.New("invalid batchMsg")
	}

	c.log.Infof("commit block (%d/%x), voteSet = %v", bm.Sequence(), bm.Key(), voteSet.Votes)
	c.BlockHeight = bm.Sequence()
	c.blockHeightC <- bm.Sequence()
	return nil
}

// CommitDone 完成batchMsg提交，通知共识开始下一个共识
func (c *Committer) CommitDone() <-chan uint64 {
	return c.blockHeightC
}
