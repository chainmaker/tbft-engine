package test

import (
	"bytes"
	tbftengine "chainmaker.org/chainmaker/tbft-engine"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
)

var _ tbftengine.NetHandler = &NetHandler{}

const (
	ginLogFile = "./gin.log"
	receiveUrl = "/receiveMsg"
)

type NetHandler struct {
	logger   *Logger
	nodeId   string
	messageC chan interface{}
	host     *tbftengine.ConsensusTBFTImpl
}

type Msg struct {
	Data []byte
}

func NewNetHandler(logger *Logger, size int, nodeId string, host *tbftengine.ConsensusTBFTImpl) *NetHandler {
	netHandler := &NetHandler{
		logger:   logger,
		messageC: make(chan interface{}, size),
		nodeId:   nodeId,
		host:     host,
	}
	go netHandler.startHttpServer()
	return netHandler
}

func (n *NetHandler) startHttpServer() {
	file, _ := os.Create(ginLogFile + "." + n.nodeId)
	gin.DefaultWriter = file
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.POST(receiveUrl, func(c *gin.Context) {
		msg := &Msg{}
		if err := c.ShouldBindJSON(msg); err != nil {
			n.logger.Errorf("received invalid msg")
			return
		}
		n.messageC <- msg.Data
		c.JSON(http.StatusOK, map[string]interface{}{"code": 200})
	})
	r.Run(n.nodeId)
}

func (n *NetHandler) Name() string {
	return NetHandlerName
}

func (n *NetHandler) Close() {
}

// notice:ListenNetMsg
func (n *NetHandler) Listen() <-chan interface{} {
	return n.messageC
}

func (n *NetHandler) BroadCastNetMsg(payload []byte, to string) error {
	if payload == nil {
		return errors.New("invalid payload")
	}

	var validators []string
	if to != "" {
		validators = append(validators, to)
	} else {
		hostValidators, _ := n.host.GetValidators()
		validators = append(validators, hostValidators...)
	}

	n.logger.Infof("ready send consensus message to %v ", validators)
	for _, v := range validators {
		// The recipient is yourself
		if v == n.nodeId {
			continue
		}
		go func(validator string) {
			msg := &Msg{
				Data: payload,
			}
			n.Post(validator, msg)
		}(v)
	}
	return nil
}

func (n *NetHandler) Post(url string, msg *Msg) (err error) {
	jsonStr, _ := json.Marshal(msg)
	realUrl := fmt.Sprintf("http://%s%s", url, receiveUrl)
	req, err := http.NewRequest("POST", realUrl, bytes.NewBuffer(jsonStr))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	client.Timeout = 8 * time.Second
	resp, err := client.Do(req)
	if err != nil || resp.StatusCode != 200 {
		n.logger.Errorf("Post to appserver failed, err = %s", err)
		return err
	}

	defer resp.Body.Close()
	respBodyB, err := ioutil.ReadAll(resp.Body)
	respBody := string(respBodyB)

	n.logger.Infof("realUrl 2 = %s", realUrl)
	var f interface{}
	err = json.Unmarshal([]byte(respBody), &f)
	if err != nil {
		n.logger.Errorf("callback post response invaild, respBody = %s", respBody)
		return err
	}
	var m = f.(map[string]interface{})
	code_, ok := m["code"]
	if ok == false {
		n.logger.Errorf("callback post response invaild,respBody = %s", respBody)
		return ErrNotExistCode
	}
	var code int = 0
	switch code_.(type) {
	case int:
		code = code_.(int)
	case string:
		code, _ = strconv.Atoi(code_.(string))
	case float64:
		code = int(code_.(float64))
	}
	if code != 200 {
		n.logger.Errorf("callback post response invaild,respBody = %s", respBody)
		return ErrInvaildCode
	}
	return nil
}
