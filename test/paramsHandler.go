package test

import (
	tbftengine "chainmaker.org/chainmaker/tbft-engine"
	"time"
)

const (
	// DefaultTimeoutPropose Timeout of waitting for a proposal before prevoting nil
	DefaultTimeoutPropose = 30 * time.Second
	// DefaultTimeoutProposeDelta Increased time delta of TimeoutPropose between rounds
	DefaultTimeoutProposeDelta = 1 * time.Second
)

type ParamsHandler struct {
}

var (
	_          tbftengine.ParamsHandler = &ParamsHandler{}
	Validators                          = []string{
		"127.0.0.1:8081",
		"127.0.0.1:8082",
		"127.0.0.1:8083",
		"127.0.0.1:8084",
	}
)

func NewParamsHandler() *ParamsHandler {
	return &ParamsHandler{}
}

func (p *ParamsHandler) Name() string {
	return ParamsHandlerName
}

func (p *ParamsHandler) Close() {
}

// GetNewParams 新的一次共识开始时调用，获取共识需要的参数
func (p *ParamsHandler) GetNewParams() (validators []string, timeoutPropose time.Duration,
	timeoutProposeDelta time.Duration, tbftBlocksPerProposer uint64, err error) {
	validators = Validators
	timeoutPropose = DefaultTimeoutPropose
	timeoutProposeDelta = DefaultTimeoutProposeDelta
	tbftBlocksPerProposer = 1

	return
}
