package test

import (
	tbft_engine "chainmaker.org/chainmaker/tbft-engine"
	"context"
	"crypto/ecdsa"
	"errors"
	"testing"
	"time"
)

func TestTBFT_CONSENSUS_ENGINE(t *testing.T) {
	var nodes []*TBFTTestImpl

	log := NewLogger()
	// new nodes
	for i := 8081; i <= 8084; i++ {
		node, err := New(i, log)
		if err == nil {
			nodes = append(nodes, node)
		} else {
			panic(err)
		}
	}
	// start nodes
	for _, node := range nodes {
		err := node.Start()
		if err != nil {
			panic(err)
		}
	}

	time.Sleep(60 * time.Second)
	for _, node := range nodes {
		err := node.Stop()
		if err != nil {
			panic(err)
		}
	}
}

var (
	defaultChanCap = 1000
	chainId        = "chain1"
	nodes          = map[int]string{
		8081: "127.0.0.1:8081",
		8082: "127.0.0.1:8082",
		8083: "127.0.0.1:8083",
		8084: "127.0.0.1:8084",
	}
	nodeIdEcdsaKeyPair  = make(map[string]*EcdsaKeyPair)
	validatorsPublicKey = make(map[string]*ecdsa.PublicKey)
)

func init() {
	for _, node := range nodes {
		privateKey, publicKey, _ := GenKeyPair()
		ecdsaKeyPair := NewEcdsaKeyPair(privateKey, publicKey)
		nodeIdEcdsaKeyPair[node] = ecdsaKeyPair
		validatorsPublicKey[node] = ecdsaKeyPair.PubKey
	}
}

type TBFTTestImpl struct {
	Id             string
	logger         *Logger
	closeC         chan struct{}
	handlerManager *HandlerManager
	engine         *tbft_engine.ConsensusTBFTImpl
}

func New(port int, log *Logger) (*TBFTTestImpl, error) {
	_, ok := nodes[port]
	if !ok {
		return nil, errors.New("invalid port")
	}
	var err error
	consensus := &TBFTTestImpl{}
	var currentHeight uint64 = 0

	var validators []string
	for _, v := range nodes {
		validators = append(validators, v)
	}

	consensus.Id = nodes[port]
	consensus.logger = log
	consensus.closeC = make(chan struct{})
	node, err := tbft_engine.NewNode(consensus.logger, consensus.Id, chainId, currentHeight, validators)
	if err != nil {
		return nil, err
	}
	size := defaultChanCap

	consensus.handlerManager = NewHandlerManager(consensus.logger, node,
		NewNetHandler(consensus.logger, size, consensus.Id, node),
		NewBatchMsgInterpreter(size, consensus.Id),
		NewVerifier(consensus.logger, nodeIdEcdsaKeyPair[nodes[port]], validatorsPublicKey),
		NewWalHandler(),
		NewCommitter(consensus.logger, size),
		NewParamsHandler(),
		NewProposalHandler(consensus.logger),
	)
	err = consensus.handlerManager.StartrHandler()
	if err != nil {
		consensus.logger.Errorf("New ConsensusTBFTImpl failed, err = %v", err)
		return nil, err
	}
	consensus.engine = node
	return consensus, nil
}

// Start starts the tbft instance with tbft-engine
func (consensus *TBFTTestImpl) Start() error {
	ctx := context.TODO()
	go consensus.procProposeState()
	go consensus.procVerifyFailTxs()
	return consensus.engine.Start(ctx)
}

// Stop implements the Stop method of ConsensusEngine interface.
func (consensus *TBFTTestImpl) Stop() error {
	consensus.handlerManager.StopHandler()
	close(consensus.closeC)
	return consensus.engine.Stop()
}

// procProposeState implements the listen ProposeState method of ConsensusEngine interface.
func (consensus *TBFTTestImpl) procProposeState() {
	consensus.logger.Infof("start procProposeState")
	loop := true

	batchMsgInterpreter, ok := consensus.handlerManager.HandlerMap[BatchMsgInterpreterName].(*BatchMsgInterpreter)
	if !ok {
		return
	}

	committer, ok := consensus.handlerManager.HandlerMap[CommitterName].(*Committer)
	if !ok {
		return
	}

	for loop {
		select {
		case isProposer := <-consensus.engine.ProposeState():
			status := consensus.engine.Status()
			consensus.logger.Infof("[%s](%d/%d/%s) sendProposeState isProposer: %v",
				consensus.Id, status.Sequence, status.Round, status.Step, isProposer)
			if isProposer {
				batchMsgInterpreter.SetSequence(committer.BlockHeight)
			}
		case <-consensus.closeC:
			loop = false
		}
	}
}

// procVerifyFailTxs implements the listen VerifyFailTxs method of ConsensusEngine interface.
func (consensus *TBFTTestImpl) procVerifyFailTxs() {
	consensus.logger.Infof("start procVerifyFailTxs")
	loop := true
	for loop {
		select {
		case rollBack := <-consensus.engine.RollBack():
			status := consensus.engine.Status()
			consensus.logger.Infof("[%s](%d/%d/%s) delete invalidTxs [%v]",
				consensus.Id, status.Sequence, status.Round, status.Step, rollBack.VerifyFailTxs)
		case <-consensus.closeC:
			loop = false
		}
	}
}
