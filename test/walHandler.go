package test

import (
	tbftengine "chainmaker.org/chainmaker/tbft-engine"
)

// WalHandler 实现读写共识消息的wal操作
type WalHandler struct {
}

var _ tbftengine.WalHandler = &WalHandler{}

func NewWalHandler() *WalHandler {
	return &WalHandler{}
}

func (w *WalHandler) Write(walEntry *tbftengine.WalEntry) error {
	return nil
}

// ReadLast 读取一个最新的wal内容
func (w *WalHandler) ReadLast() (*tbftengine.WalEntry, error) {
	return nil, nil
}

func (w *WalHandler) Name() string {
	return WalHandlerName
}

func (w *WalHandler) Close() {
}
