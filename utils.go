/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package tbft_engine

import (
	"encoding/base64"
	"errors"
	"fmt"
	"sync"
	"time"

	tbftpb "chainmaker.org/chainmaker/tbft-engine/pb"
)

// VerifyQcFromVotes verifies whether the signatures in votes
// verify that the maj32 of the votes
// error when verify successfully, and return corresponding error
// when failed.
func VerifyQcFromVotes(logger Logger, verifier Verifier, vs []*tbftpb.Vote,
	validators *validatorSet, voteType tbftpb.VoteType) (*VoteSet, error) {
	if vs == nil || len(vs) <= 0 || voteType != vs[0].Type {
		logger.Warnf("invalid []*tbftpb.Vote, [%v]", vs)
		return nil, fmt.Errorf("invalid []*tbftpb.Vote")
	}
	voteSet := NewVoteSet(logger, vs[0].Type, vs[0].Sequence, vs[0].Round, validators)
	for _, v := range vs {
		_, err := voteSet.AddVote(v, false)
		if err != nil {
			return nil, err
		}
	}

	hash, ok := voteSet.twoThirdsMajority()
	// we need a QC
	if !ok {
		return nil, fmt.Errorf("votes qc without majority, ok = %v, type = %v, hash = %x", ok, voteSet.Type, hash)
	}

	hashStr := base64.StdEncoding.EncodeToString(hash)
	blockVotes := voteSet.VotesByBlock[hashStr]
	//
	err := verifyVotes(blockVotes.Votes, verifier)
	if err != nil {
		clog.Debugf("verify vote signatures failed, %v", err)
		return nil, err
	}

	clog.Debugf("verify votes success")
	return voteSet, nil
}

// VerifyRoundQc verifies whether the signatures in roundQC
// verify that the Qc is nil hash and the maj32 of the voteSet
// error when verify successfully, and return corresponding error
// when failed.
func VerifyRoundQc(logger Logger, verifier Verifier,
	validators *validatorSet, roundQC *tbftpb.RoundQC) error {
	if roundQC == nil && roundQC.Precommits == nil {
		return fmt.Errorf("invalid roundQC")
	}

	voteSet := NewVoteSetFromProto(logger, roundQC.Precommits, validators)
	hash, ok := voteSet.twoThirdsMajority()
	// we need a QC
	if !ok || roundQC.Precommits.Type != voteSet.Type {
		return fmt.Errorf("qc without majority, ok = %v, type = %v, hash = %x", ok, roundQC.Precommits.Type, hash)
	}

	hashStr := base64.StdEncoding.EncodeToString(hash)
	blockVotes := voteSet.VotesByBlock[hashStr]
	//
	err := verifyVotes(blockVotes.Votes, verifier)
	if err != nil {
		clog.Debugf("verify round qc signatures failed, %v", err)
		return err
	}

	clog.Debugf("verify round qc signatures success")
	return nil
}

func verifyVotes(votes map[string]*tbftpb.Vote, verifier Verifier) error {
	if votes == nil {
		return errors.New("invalid votes")
	}

	wg := sync.WaitGroup{}
	// no lock is needed to protect retErr, get any error code
	var retErr error
	for _, value := range votes {
		wg.Add(1)
		go func(v *tbftpb.Vote) {
			var err error
			defer func() {
				wg.Done()
				if err != nil {
					retErr = err
				}
			}()

			err = verifier.VerifyVote(v)
			if err != nil {
				clog.Infof("verify signatures vote(%s) error: %v", v.Voter, err)
				return
			}
		}(value)
	}

	wg.Wait()

	return retErr
}

// CurrentTimeMillisSeconds return current unix timestamp in milliseconds
func CurrentTimeMillisSeconds() int64 {
	return time.Now().UnixNano() / 1e6
}
